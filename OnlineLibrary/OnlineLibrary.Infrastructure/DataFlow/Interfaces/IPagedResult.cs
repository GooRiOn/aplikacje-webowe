﻿using System.Security.Cryptography.X509Certificates;

namespace OnlineLibrary.Infrastructure.DataFlow
{
    public interface IPagedResult<TResult>
    {
        TResult Result { get; set; }
        int TotalPagesNumber { get; set; }
    }
}