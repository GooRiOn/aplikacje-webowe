﻿namespace OnlineLibrary.Infrastructure.DataFlow
{
    public class PagedResult<TResult> : IPagedResult<TResult>
    {
        public TResult Result { get; set; }
        public int TotalPagesNumber { get; set; }
    }
}