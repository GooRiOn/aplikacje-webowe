﻿using System.Linq;
using OnlineLibrary.Infrastructure.Dtos;
using OnlineLibrary.Infrastructure.Entities;

namespace OnlineLibrary.Infrastructure.QueryExtensions
{
    public static class QueryExtensions
    {
        public static IQueryable<BookDto> ToBookDto(this IQueryable<BookEntity> that)
        {
            return that.Select(b => new BookDto
            {
                Id = b.Id,
                Title = b.Title,
                Genre = b.Genre,
                Iso = b.Iso,
                Description = b.Description,
                Quantity = b.Quantity,
                AuthorId = b.AuthorId,
                AuthorName = b.Author.Name,
                AuthorSurname = b.Author.Surname,
                GraphicId = b.GraphicId
            });
        }
    }
}