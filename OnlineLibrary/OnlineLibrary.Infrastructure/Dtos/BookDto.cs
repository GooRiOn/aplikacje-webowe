﻿using OnlineLibrary.Infrastructure.Enums;

namespace OnlineLibrary.Infrastructure.Dtos
{
    public class BookDto
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public BookGenreEnum Genre { get; set; }

        public string Iso { get; set; }

        public string Description { get; set; }

        public int Quantity { get; set; }
        public int AuthorId { get; set; }

        public string AuthorName { get; set; }

        public string AuthorSurname { get; set; }
        public int GraphicId { get; set; }

    }
}