﻿namespace OnlineLibrary.Infrastructure.Dtos
{
    public class AdminUserListDto
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public bool IsLocked { get; set; }
    }
}
