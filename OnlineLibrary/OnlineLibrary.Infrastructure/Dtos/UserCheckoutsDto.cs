﻿using System;
using OnlineLibrary.Infrastructure.Enums;

namespace OnlineLibrary.Infrastructure.Dtos
{
    public class UserCheckoutsDto
    {
        public int Id { get; set; }
        public string BookTitle { get; set; }
        public DateTime CheckoutDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public BookCheckoutStatusEnum Status { get; set; }
    }
}