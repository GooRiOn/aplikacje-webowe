﻿using System.Collections.Generic;
using System.Security.Policy;

namespace OnlineLibrary.Infrastructure.Dtos
{
    public class UserInfoDto
    {
        public string Username { get; set; }

        public List<string> UserRoles { get; set; }

        public bool IsLoggedIn { get; set; }
    }
}
