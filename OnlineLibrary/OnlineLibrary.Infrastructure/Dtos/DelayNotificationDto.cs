﻿using OnlineLibrary.Infrastructure.Entities;

namespace OnlineLibrary.Infrastructure.Dtos
{
    public class DelayNotificationDto
    {
        public UserEntity User { get; set; }
        public int DaysDueNumber { get; set; }
        
    }
}