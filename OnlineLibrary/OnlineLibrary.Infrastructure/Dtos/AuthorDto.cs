﻿namespace OnlineLibrary.Infrastructure.Dtos
{
    public class AuthorDto
    {
        public int Id { get; set; }
        public string Names { get; set; }
    }
}