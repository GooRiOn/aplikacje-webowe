﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Infrastructure.Dtos
{
    public class SimilarBookDto
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public string AuthorNames { get; set; }

        public int GraphicId { get; set; }
    }
}
