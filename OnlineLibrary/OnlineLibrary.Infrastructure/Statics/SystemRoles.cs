﻿namespace OnlineLibrary.Infrastructure.Statics
{
    public static class SystemRoles
    {
        public static readonly string Admin = "admin";
        public static readonly string Librarian = "librarian";

    }
}
