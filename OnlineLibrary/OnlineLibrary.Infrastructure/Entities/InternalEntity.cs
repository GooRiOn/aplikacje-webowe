﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OnlineLibrary.Infrastructure.Entities
{
    public interface IInternalEntity
    {
        DateTime CreatedDate { get; }
        DateTime UpdatedDate { get; }

        void SetCreatedDate(DateTime createdDate);
        void SetUpdatedDate(DateTime updatedDate);
    }

    public interface ISoftDeletable
    {
        bool IsActive { get; set; }
        void SoftDelete();
    }

    public abstract class InternalEntity : IInternalEntity, ISoftDeletable
    {
        protected InternalEntity()
        {
            IsActive = true;
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        public DateTime CreatedDate { get; private set; }

        public DateTime UpdatedDate { get; private set; }

        public bool IsActive { get; set; }

        void IInternalEntity.SetCreatedDate(DateTime createdDate)
        {
            CreatedDate = createdDate;
        }

        void IInternalEntity.SetUpdatedDate(DateTime updatedDate)
        {
            UpdatedDate = updatedDate;
        }

        void ISoftDeletable.SoftDelete()
        {
            IsActive = false;
        }
        
    }
}
