﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnlineLibrary.Infrastructure.Entities
{
    [Table("Authors",Schema = "dbo")]
    public class AuthorEntity : InternalEntity
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string Description { get; set; }

        public int Age { get; set; }
    }
}
