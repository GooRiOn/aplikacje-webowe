﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;

namespace OnlineLibrary.Infrastructure.Entities
{
    public interface ILockable
    {
        bool IsLocked { get; }
        void Lock();
        void Unlock();
    }

    public class UserEntity : IdentityUser, IInternalEntity, ILockable
    {
        public UserEntity()
        {
            IsLocked = false;
            CreatedDate = DateTime.Now;
            UpdatedDate = DateTime.Now;
        }

        public string FirstName { get; set; }
        public string Surname { get; set; }

        public DateTime CreatedDate { get; private set; }
        public DateTime UpdatedDate { get; private set; }

        public bool IsLocked { get; private set; }

        void ILockable.Lock()
        {
            IsLocked = true;
        }

        void ILockable.Unlock()
        {
            IsLocked = false;
        }

        void IInternalEntity.SetCreatedDate(DateTime createdDate)
        {
            CreatedDate = createdDate;
        }

        void IInternalEntity.SetUpdatedDate(DateTime updatedDate)
        {
            UpdatedDate = updatedDate;
        }
    }
}