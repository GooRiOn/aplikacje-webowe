﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;

namespace OnlineLibrary.Infrastructure.Entities
{
    [Table("Graphics", Schema = "dbo")]
    public class GraphicEntity: InternalEntity
    {
        public byte[] Content { get; set; }
        public string FileName { get; set; }
    }
}