﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OnlineLibrary.Infrastructure.Enums;

namespace OnlineLibrary.Infrastructure.Entities
{
    [Table("BookCheckouts", Schema = "dbo")]
    public class BookCheckoutEntity: InternalEntity
    {
        public BookCheckoutEntity()
        {
            Status = BookCheckoutStatusEnum.RequestSent;
            ExpirationDate = DateTime.UtcNow.AddDays(30);
        }

        [Key]
        public int Id { get; set; }

        public DateTime ExpirationDate { get; set; }

        public BookCheckoutStatusEnum Status { get; set; }

        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public UserEntity User { get; set; }

        public int BookId { get; set; }

        [ForeignKey("BookId")]
        public BookEntity Book { get; set; }
    }
}