﻿using System.ComponentModel.DataAnnotations.Schema;
using OnlineLibrary.Infrastructure.Enums;
using System.ComponentModel.DataAnnotations;

namespace OnlineLibrary.Infrastructure.Entities
{
    [Table("Books", Schema = "dbo")]
    public class BookEntity : InternalEntity
    {
        public string Title { get; set; }

        public BookGenreEnum Genre { get; set; }

        public string Iso { get; set; }

        public string Description { get; set; }

        public int Quantity { get; set; }

        public int AuthorId { get; set; }

        [ForeignKey("AuthorId")]
        public AuthorEntity Author { get; set; }

        public int GraphicId { get; set; }

        [ForeignKey("GraphicId")]
        public GraphicEntity Graphic { get; set; }
    }
}
