﻿
namespace OnlineLibrary.Infrastructure.Enums
{
    public enum BookGenreEnum
    {
        Fantasy = 1,
        Thriller = 2,
        Drama = 3,
        Commedy = 4,
        All = 5
    }
}
