﻿namespace OnlineLibrary.Infrastructure.Enums
{
    public enum BookCheckoutStatusEnum
    {
        RequestSent = 1,
        Awaiting = 2,
        Active = 3,
        Delayed = 4
    }
}