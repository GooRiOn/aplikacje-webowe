﻿using System.Collections.Generic;
using OnlineLibrary.Infrastructure.Enums;
using System.Drawing;
using System.IO;

namespace OnlineLibrary.Infrastructure.Helpers
{
    public static class GraphicHelper
    {
         private static readonly Dictionary<GraphicSizeEnum, Size> _graphicSizes = new Dictionary<GraphicSizeEnum, Size>
         {
             {GraphicSizeEnum.BookFrontCover, new Size(300, 400)}
         };

        public static Image ResizeGraphic(Image graphic, GraphicSizeEnum sizeKind)
        {
            Size graphicSize;

            if (!_graphicSizes.TryGetValue(sizeKind, out graphicSize))
               throw new InvalidDataException("Graphic size not found.");

            return new Bitmap(graphic, graphicSize);
        }

        public static byte[] GraphicToBytes(Image graphic)
        {
            var memoryStream = new MemoryStream();
            graphic.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Gif);
            return memoryStream.ToArray();
        }

        public static Image BytesToGraphic(byte[] graphicBytes)
        {
            var memoryStream = new MemoryStream(graphicBytes);
            Image returnImage = Image.FromStream(memoryStream);
            return returnImage;
        }

        public static string GetContentType(string fileName)
        {
            var ext = Path.GetExtension(fileName).ToLower().Replace(".", "");
            return "image/" + ext;
        }
    }
}