﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Repository.Repo.Interfaces;

namespace OnlineLibrary.Repository.Repo
{
    public abstract class GenericRepo<TEntity> :IGenericRepo<TEntity> where TEntity : class, IInternalEntity
    {
        private readonly DbContext _context;
        public IQueryable<TEntity> Query { get; private set; }

        protected GenericRepo(DbContext db)
        {
            _context = db;
            Query = db.Set<TEntity>();
        }

        public IQueryable<TEntity> Get()
        {
            return Query;
        }

        public TEntity Add(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
            return entity;
        }

        public void Update(TEntity entity)
        {
            var internalEntity = (IInternalEntity) entity;

            internalEntity.SetUpdatedDate(DateTime.Now);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(TEntity entity)
        {
            var internalEntity = (IInternalEntity) entity;
            
            internalEntity.SetUpdatedDate(DateTime.Now);

            if (entity is ISoftDeletable)
            {
                var softDeletableEntity = (ISoftDeletable) entity;
                softDeletableEntity.SoftDelete();
                _context.Entry(entity).State = EntityState.Modified;
            }
            else
                _context.Set<TEntity>().Remove(entity);
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public Task CommitAsync()
        {
             return _context.SaveChangesAsync();
        }
    }
}
