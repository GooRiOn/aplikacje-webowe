﻿using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Repository.Repo.Interfaces;

namespace OnlineLibrary.Repository.Repo
{
    public class AuthorRepo : GenericRepo<AuthorEntity>, IAuthorRepo
    {
        public AuthorRepo(Ctx db)
            :base(db)
        {
        }
    }
}
