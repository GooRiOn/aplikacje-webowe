﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Repository.Repo.Interfaces;

namespace OnlineLibrary.Repository.Repo
{
    public class UserRepo : GenericRepo<UserEntity>, IUserRepo
    {
        public UserRepo(Ctx db)
            :base(db)
        {
        }
    }
}
