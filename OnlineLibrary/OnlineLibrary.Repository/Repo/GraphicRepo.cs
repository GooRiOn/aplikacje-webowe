﻿using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Repository.Repo.Interfaces;

namespace OnlineLibrary.Repository.Repo
{
    public class GraphicRepo: GenericRepo<GraphicEntity>, IGraphicRepo
    {

        public GraphicRepo(Ctx db)
            :base(db)
        {
            
        }
         
    }
}