﻿using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Repository.Repo.Interfaces;

namespace OnlineLibrary.Repository.Repo
{
    public class BookCheckoutRepo: GenericRepo<BookCheckoutEntity>, IBookCheckoutRepo
    {
        public BookCheckoutRepo(Ctx db)
            :base(db)
        {
            
        }
    }
}