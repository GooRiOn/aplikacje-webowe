﻿using OnlineLibrary.Infrastructure.Entities;

namespace OnlineLibrary.Repository.Repo.Interfaces
{
    public interface IBookRepo : IGenericRepo<BookEntity>
    {
    }
}