﻿using OnlineLibrary.Infrastructure.Entities;

namespace OnlineLibrary.Repository.Repo.Interfaces
{
    public interface IAuthorRepo : IGenericRepo<AuthorEntity>
    {
    }
}