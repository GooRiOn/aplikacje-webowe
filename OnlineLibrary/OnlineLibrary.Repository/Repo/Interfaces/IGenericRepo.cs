﻿using System.Linq;
using System.Threading.Tasks;
using OnlineLibrary.Infrastructure.Entities;

namespace OnlineLibrary.Repository.Repo.Interfaces
{
    public interface IGenericRepo<TEntity> where TEntity : IInternalEntity
    {
        IQueryable<TEntity> Query { get;}

        IQueryable<TEntity> Get();
        TEntity Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Commit();
        Task CommitAsync();
    }
}