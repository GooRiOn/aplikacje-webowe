﻿using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Repository.Repo.Interfaces;

namespace OnlineLibrary.Repository.Repo
{
    public class BookRepo : GenericRepo<BookEntity>, IBookRepo
    {
        public BookRepo(Ctx db)
            :base(db)
        {
        }
    }
}
