﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using OnlineLibrary.Infrastructure.Entities;

namespace OnlineLibrary.Repository
{
    public class Ctx : IdentityDbContext<UserEntity>
    {
        public Ctx()
            : base("OnlineLibrary")
        {
        }

        static Ctx()
        {
            Database.SetInitializer(new CtxSeed());
        }

        public virtual DbSet<BookEntity> Books { get; set; }
        public virtual DbSet<AuthorEntity> Authors { get; set; }
        public virtual DbSet<BookCheckoutEntity> BookCheckouts { get; set; }
        public virtual DbSet<GraphicEntity> Graphics { get; set; }
    }
}
