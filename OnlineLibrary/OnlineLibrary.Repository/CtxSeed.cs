﻿using System;
using System.Collections.Generic;
using OnlineLibrary.Infrastructure.Entities;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using OnlineLibrary.Infrastructure.Enums;
using OnlineLibrary.Infrastructure.Helpers;
using OnlineLibrary.Infrastructure.Statics;
using OnlineLibrary.Repository.Auth;
using System.Web;

namespace OnlineLibrary.Repository
{
    public class CtxSeed : CreateDatabaseIfNotExists<Ctx>
    {
        protected override void Seed(Ctx db)
        {
            db.Authors.AddRange(new List<AuthorEntity>
            {
                new AuthorEntity{Name = "John", Surname = "Smith",Age = 20,Description = ""},
                new AuthorEntity{Name = "Dean", Surname = "Brown",Age = 20,Description = ""},
                new AuthorEntity{Name = "William", Surname = "Clinton",Age = 20,Description = ""},
                new AuthorEntity{Name = "Sam", Surname = "Winchester",Age = 20,Description = ""},
                new AuthorEntity{Name = "Merry", Surname = "Jane",Age = 20,Description = ""},
            });

            
            
            var roleManager = new RoleManager(db);

            roleManager.Create(new IdentityRole {Name = SystemRoles.Admin});
            roleManager.Create(new IdentityRole { Name = SystemRoles.Librarian });

            var adminRole = roleManager.GetAdminRole();
            var adminUser = CreateUser("admin", "test123",db);

            adminRole.Users.Add(new IdentityUserRole {UserId = adminUser.Id, RoleId = adminRole.Id});

            CreateSampleUsers(db);
            CreateSampleBooks(db);

            db.SaveChanges();

        }

        private UserEntity CreateUser(string username, string password, Ctx db)
        {
            var user = new UserEntity {UserName = username};

            var userAccountManager = new UserAccountManager(db);

            userAccountManager.Create(user,password);

            userAccountManager.Dispose();

            return user;
        }

        private void CreateSampleUsers(Ctx db)
        {
            var userAccountManager = new UserAccountManager(db);

            for(var i = 0; i < 100; ++i)
            {
                var user = new UserEntity { UserName = "User" + i };
                userAccountManager.Create(user, "test123");
            }
        }

        private void CreateSampleBooks(Ctx db)
        {
            var random = new Random();
            var books = new List<BookEntity>();


            var titles = new[] { "The girl on the train", "Red Queen", "The Nightingale", "Go Set a Watchman", "All the Bright Places", "The Heir", "An Ember in the Ashes", "Winter", "Grey", "Confess", "I Was Here", "Queen of Shadows", "Never Never", "Why not Me?", "	Luckiest Girl Alive", "Six of Crows", "Carry On", "Dead Wake: The Last Crossing of the Lusitania", "A Darker Shade of Magic", "At the Water's Edge", "P.S. I Still Love You", "Modern Romance", "In the Unlikely Event", "The Sword of Summer", "Fairest", "After You", "Uprooted" };
            var graphicNames = new[] { "theGirlOnTheTrain.jpg", "redQueen.jpg", "theNightingale.jpg", "goSet.jpg", "allTheBrightPlaces.jpg", "theHeir.jpg", "anEmber.jpg", "winter.jpg", "grey.jpg", "confess.jpg", "iWasHere.jpg", "queenOfShadows.jpg", "neverNever.jpg", "whyNotMe.jpg", "luckiestGirl.jpg", "sixOfCrows.jpg", "carryOn.jpg", "deadWake.jpg", "aDarkerShade.jpg", "watersEdge.jpg", "pSILoveYou.jpg", "modernRomance.jpg", "unlikelyEvent.jpg", "swordOfSummer.jpg", "fairest.jpg", "afterYou.jpg", "uprooted.jpg"};

            for (var i = 0; i < titles.Length; ++i)
            {

                var bookCover = Image.FromFile(String.Concat(HttpContext.Current.Server.MapPath("~/Content/Images/"),graphicNames[i]));
                bookCover = GraphicHelper.ResizeGraphic(bookCover, GraphicSizeEnum.BookFrontCover);
                var bookCoverBytes = GraphicHelper.GraphicToBytes(bookCover);

                var graphicEntity = new GraphicEntity
                {
                    Content = bookCoverBytes,
                    FileName = graphicNames[i]
                };
                

                books.Add(new BookEntity
                {
                    Title = titles[i],
                    Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu metus id est ultrices lobortis. Suspendisse vel ipsum vitae sapien sagittis maximus non sit amet ligula. Proin nec diam et arcu varius elementum. Nullam vel luctus mi. Praesent viverra sem ut dolor porttitor, eu porta lectus aliquet. Nam quis iaculis arcu. Nulla velit diam, mattis non tempus ac, dapibus et ipsum. Nunc dictum libero vitae tristique congue. Aenean eu metus ipsum. Cras molestie velit at nulla tincidunt, in dignissim risus fringilla. Nullam quis purus ipsum. Proin mollis nisi imperdiet sapien sollicitudin rutrum. Nulla facilisi. Ut pretium libero quis hendrerit viverra. Maecenas ornare odio justo, sit amet hendrerit sem ornare ac.",
                    Iso = "978-3-16-148410-0",
                    Genre = (BookGenreEnum)random.Next(1,4),
                    Quantity = 10,
                    AuthorId = random.Next(1, 5),
                    Graphic = graphicEntity
                });
            }

            db.Books.AddRange(books);
        }
    }
}
