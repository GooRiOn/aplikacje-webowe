﻿using System.Data.Entity;
using Microsoft.Practices.Unity;
using OnlineLibrary.Repository.Auth;
using OnlineLibrary.Repository.Auth.Interfaces;
using OnlineLibrary.Repository.Repo;
using OnlineLibrary.Repository.Repo.Interfaces;

namespace OnlineLibrary.Repository.DependencyInjection
{
    public class DependencyInjectionRegistration
    {
        public static void RegisterType(IUnityContainer container)
        {
            container.RegisterType<DbContext, Ctx>(new PerResolveLifetimeManager());
            container.RegisterType<IUserAccountManager, UserAccountManager>(new PerResolveLifetimeManager());
            container.RegisterType<IUserRepo, UserRepo>();
            container.RegisterType<IRoleManager, RoleManager>(new PerResolveLifetimeManager());
            container.RegisterType<IAuthorRepo, AuthorRepo>();
            container.RegisterType<IBookRepo, BookRepo>();
            container.RegisterType<IBookCheckoutRepo, BookCheckoutRepo>();
            container.RegisterType<IGraphicRepo, GraphicRepo>();
        }
    }
}
