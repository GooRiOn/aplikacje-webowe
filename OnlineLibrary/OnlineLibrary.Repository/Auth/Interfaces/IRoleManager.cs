﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace OnlineLibrary.Repository.Auth.Interfaces
{
    public interface IRoleManager
    {
        IdentityRole GetAdminRole();
        Task<IdentityRole> GetAdminRoleAsync();
        IdentityRole GetLibrarianRole();
        Task<IdentityRole> GetLibrarianRoleAsync();
        IQueryable<IdentityRole> GetRoles();
        IdentityRole GetRoleByName(string name);
    }
}