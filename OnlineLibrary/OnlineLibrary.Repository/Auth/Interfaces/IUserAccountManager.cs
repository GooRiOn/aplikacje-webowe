﻿using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using OnlineLibrary.Infrastructure.Entities;

namespace OnlineLibrary.Repository.Auth.Interfaces
{
    public interface IUserAccountManager
    {
        Task<UserEntity> FindByNameAsync(string username);
        Task<ClaimsIdentity> CreateIdentityAsync(UserEntity user, string authenticationType);
        Task<IdentityResult> ChangePasswordAsync(string userId, string currentPassword, string newPassword);
        Task<IdentityResult> CreateAsync(UserEntity user, string password);
        Task<UserEntity> FindByIdentityAsync(IIdentity identity);
    }
}