﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using OnlineLibrary.Infrastructure.Entities;

namespace OnlineLibrary.Repository.Auth
{
    public class UserStore<TDbContext> : Microsoft.AspNet.Identity.EntityFramework.UserStore<UserEntity>,
       IUserLockoutStore<UserEntity, string> where TDbContext : IdentityDbContext<UserEntity>,
       new()
    {
        public UserStore(TDbContext dbContext)
            : base(dbContext)
        {
        }

        public UserStore()
            : base(new TDbContext())
        {

        }
    }
}
