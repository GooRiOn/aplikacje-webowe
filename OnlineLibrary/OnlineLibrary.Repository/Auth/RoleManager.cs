﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Practices.Unity;
using OnlineLibrary.Infrastructure.Statics;
using OnlineLibrary.Repository.Auth.Interfaces;

namespace OnlineLibrary.Repository.Auth
{
    public class RoleManager : RoleManager<IdentityRole>, IRoleManager
    {
        [InjectionConstructor]
        public RoleManager(RoleStore<Ctx> store) : base(store)
        {
        }

        public RoleManager(Ctx db) : base(new RoleStore<Ctx>(db))
        {
            
        }

        public IdentityRole GetAdminRole()
        {
            return this.FindByName(SystemRoles.Admin);
        }

        public async Task<IdentityRole> GetAdminRoleAsync()
        {
            return await FindByNameAsync(SystemRoles.Admin);
        }

        public IdentityRole GetLibrarianRole()
        {
            return this.FindByName(SystemRoles.Librarian);
        }

        public async Task<IdentityRole> GetLibrarianRoleAsync()
        {
            return await FindByNameAsync(SystemRoles.Librarian);
        }

        public IQueryable<IdentityRole> GetRoles()
        {
            return Roles;
        }

        public IdentityRole GetRoleByName(string name)
        {
            return Roles.FirstOrDefault(r => r.Name == name);
        }
    }
}
