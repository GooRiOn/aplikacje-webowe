﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace OnlineLibrary.Repository.Auth
{
    public class RoleStore<TDbContext> : Microsoft.AspNet.Identity.EntityFramework.RoleStore<IdentityRole> where TDbContext : DbContext, new()
    {
        public RoleStore(TDbContext dbContext)
            : base(dbContext)
        {
        }

        public RoleStore()
            : base(new TDbContext())
        {

        }
    }
}
