﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Practices.Unity;
using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Repository.Auth.Interfaces;

namespace OnlineLibrary.Repository.Auth
{
    public class UserAccountManager : UserManager<UserEntity>, IUserAccountManager
    {
        public UserAccountManager(UserStore<Ctx> store) : base(store)
        {
            UserLockoutEnabledByDefault = true;
            MaxFailedAccessAttemptsBeforeLockout = 4;
            DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(15);
        }

        [InjectionConstructor]
        public UserAccountManager(Ctx ctx)
            : this(new UserStore<Ctx>(ctx))
        {
        }
        
        public async Task<UserEntity> FindByNameAsync(string username)
        {
            return await base.FindByNameAsync(username);
        }

        public async override Task<ClaimsIdentity> CreateIdentityAsync(UserEntity user, string authenticationType)
        {
            var task = base.CreateIdentityAsync(user, authenticationType);
            task.Wait();
            return await Task.FromResult(task.Result);
        }

        public override async Task<IdentityResult> ChangePasswordAsync(string userId,string currentPassword, string newPassword)
        {
            return await base.ChangePasswordAsync(userId, currentPassword, newPassword);
        }

        public override async Task<IdentityResult> CreateAsync(UserEntity user, string password)
        {
            return await base.CreateAsync(user, password);
        }

        public async Task<UserEntity> FindByIdentityAsync(IIdentity identity)
        {
            if (identity is ClaimsIdentity)
            {
                var claimsIdentity = (ClaimsIdentity)identity;
                var providerKeyClaim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                || String.IsNullOrEmpty(providerKeyClaim.Value))
                    return null;

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                    return await this.FindByIdAsync(providerKeyClaim.Value);

                return await this.FindAsync(new UserLoginInfo(providerKeyClaim.Issuer, providerKeyClaim.Value));
            }

            if (identity.IsAuthenticated == false)
                return null;

            return await FindByIdAsync(identity.GetUserId());
        }
    }
}
