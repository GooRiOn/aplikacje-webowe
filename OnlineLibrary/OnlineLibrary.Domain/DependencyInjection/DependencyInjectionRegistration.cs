﻿using Microsoft.Practices.Unity;
using OnlineLibrary.Domain.Services;
using OnlineLibrary.Domain.Services.Interfaces;

namespace OnlineLibrary.Domain.DependencyInjection
{
    public class DependencyInjectionRegistration
    {
        public static void RegisterType(IUnityContainer container)
        {
            Repository.DependencyInjection.DependencyInjectionRegistration.RegisterType(container);

            container.RegisterType<IAuthorService, AuthorService>();
            container.RegisterType<IBookService, BookService>();
            container.RegisterType<IUserService, UserService>(new PerResolveLifetimeManager());
            container.RegisterType<IBookCheckoutService, BookCheckoutService>();
            container.RegisterType<IGraphicService, GraphicService>();
        }
    }
}
