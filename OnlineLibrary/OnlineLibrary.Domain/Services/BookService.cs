﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using OnlineLibrary.Domain.Services.Interfaces;
using OnlineLibrary.Infrastructure.DataFlow;
using OnlineLibrary.Infrastructure.Dtos;
using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Infrastructure.Enums;
using OnlineLibrary.Infrastructure.QueryExtensions;
using OnlineLibrary.Repository.Repo.Interfaces;

namespace OnlineLibrary.Domain.Services
{
    public class BookService : EntityService<BookEntity,IBookRepo>, IBookService
    {
        public BookService(IBookRepo repo)
            :base(repo)
        {
        }

        public async Task<IPagedResult<List<BookDto>>> GetBooksPageAsync(int pageNumber, BookGenreEnum genre, int pageSize = 10)
        {
            var qBooks = Repo.Query.Where(b => b.IsActive);

            if (genre != BookGenreEnum.All)
                qBooks = qBooks.Where(b => b.Genre == genre);

            var skippedDataSize = (pageNumber - 1) * pageSize;
            var totalPagesNumber = (int)Math.Ceiling((double)qBooks.Count() / pageSize);

            var pageResult = await qBooks.ToBookDto().OrderBy(b => b.Title).Skip(skippedDataSize).Take(pageSize).ToListAsync();

            return new PagedResult<List<BookDto>>
            {
                Result = pageResult,
                TotalPagesNumber = totalPagesNumber
            };
        }

        public async Task<BookDto> GetBookByIdAsync(int id)
        {
            return await Repo.Query.Where(b => b.Id == id).ToBookDto().FirstOrDefaultAsync();
        }

        public async Task<List<BookDto>> GetBooksByTitleAsync(string title)
        {
            return await Repo.Query.Where(b => b.Title.ToLower().Contains(title.ToLower())).ToBookDto().Take(10).ToListAsync();
        }

        public async Task<bool> UpdateBook(BookDto dto)
        {
            var book = Repo.Query.FirstOrDefault(b => b.Id == dto.Id);

            if (book == null)
                return false;

            book.Title = dto.Title;
            book.Iso = dto.Iso;
            book.Genre = dto.Genre;
            book.Quantity = dto.Quantity;
            book.Description = dto.Description;
            book.AuthorId = dto.AuthorId;

            Repo.Update(book);
            await Repo.CommitAsync();

            return true;
        }

        public async Task<bool> RemoveBookAsync(int id)    
        {
            var book = Repo.Query.FirstOrDefault(b => b.Id == id);

            if (book == null)
                return false;

            Repo.Delete(book);
            await Repo.CommitAsync();

            return true;
         }

        public async Task AddBookAsync(BookEntity book)
        {
            Repo.Add(book);

            await Repo.CommitAsync();
        }

        public async Task<List<SimilarBookDto>> GetSimilarBookByGenre(BookGenreEnum genre, int bookId)
        {
            return await Repo.Query.Where(b => b.Genre == genre && b.Id != bookId).Select(b => new SimilarBookDto
            {
                Id = b.Id,
                Title = b.Title,
                AuthorNames = String.Concat(b.Author.Name, " ", b.Author.Surname),
                GraphicId = b.GraphicId
            }).Take(3).ToListAsync();
        }
    }
}
