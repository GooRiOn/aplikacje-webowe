﻿using System.Linq;
using System.Threading.Tasks;
using OnlineLibrary.Domain.Services.Interfaces;
using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Repository.Repo.Interfaces;

namespace OnlineLibrary.Domain.Services
{
    public abstract class EntityService<TEntity, TRepo> : IEntityService<TEntity> where TEntity : class, IInternalEntity where TRepo : IGenericRepo<TEntity>
    {
        public TRepo Repo { get; set; }

        protected EntityService(TRepo repo)
        {
            Repo = repo;
        }

        public IQueryable<TEntity> Get()
        {
            return Repo.Get();
        }
        
        public TEntity Add(TEntity entity)
        {
            return Repo.Add(entity);
        }

        public void Update(TEntity entity)
        {
            Repo.Update(entity);
        }

        public void Delete(TEntity entity)
        {
            Repo.Delete(entity);
        }
      
        public void Commit()
        {
            Repo.Commit();
        }

        public Task CommitAsync()
        {
            return Repo.CommitAsync();
        }
    }
}
