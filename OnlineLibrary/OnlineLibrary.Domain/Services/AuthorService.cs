﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using OnlineLibrary.Domain.Services.Interfaces;
using OnlineLibrary.Infrastructure.Dtos;
using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Repository.Repo.Interfaces;

namespace OnlineLibrary.Domain.Services
{
    public class AuthorService : EntityService<AuthorEntity,IAuthorRepo>, IAuthorService
    {
        public AuthorService(IAuthorRepo repo)
            :base(repo)
        {
        }

        public async Task<List<AuthorDto>> GetAuthorsAsync()
        {
            return await Repo.Query.Where(a => a.IsActive).Select(a => new AuthorDto
            {
                Id = a.Id,
                Names = String.Concat(a.Name," ", a.Surname)
            }).ToListAsync();
        }
    }
}
