﻿using System.Data.Entity;
using System.Drawing;
using System.Threading.Tasks;
using OnlineLibrary.Domain.Services.Interfaces;
using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Infrastructure.Enums;
using OnlineLibrary.Infrastructure.Helpers;
using OnlineLibrary.Repository.Repo.Interfaces;

namespace OnlineLibrary.Domain.Services
{
    public class GraphicService: EntityService<GraphicEntity, IGraphicRepo>, IGraphicService
    {
        public GraphicService(IGraphicRepo repo) 
            :base(repo)
        {

        }

        public async Task<GraphicEntity> GetGraphicByIdAsync(int id)
        {
            return await Repo.Query.FirstAsync(g => g.Id == id);
        }

        public async Task<GraphicEntity> AddGraphicAsync(Image graphic, string fileName, GraphicSizeEnum sizeKind)
        {
            graphic = GraphicHelper.ResizeGraphic(graphic, sizeKind);

            var entity = new GraphicEntity
            {
                Content = GraphicHelper.GraphicToBytes(graphic),
                FileName = fileName
            };

            Repo.Add(entity);
            await Repo.CommitAsync();

            return entity;
        }
    }
}