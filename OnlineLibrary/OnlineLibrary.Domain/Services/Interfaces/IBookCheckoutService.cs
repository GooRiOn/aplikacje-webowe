﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OnlineLibrary.Infrastructure.DataFlow;
using OnlineLibrary.Infrastructure.Dtos;
using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Infrastructure.Enums;

namespace OnlineLibrary.Domain.Services.Interfaces
{
    public interface IBookCheckoutService: IEntityService<BookCheckoutEntity>
    {
        Task<DelayNotificationDto> GetDelayNotificationDtoAsync(int checkoutId);
        Task<bool> CheckoutBookAsync(int bookId, string userId);
        Task<List<UserCheckoutsDto>> GetUserCheckoutsAsync(string userId);
        Task<bool> RemoveCheckoutAsync(int checkoutId, string userId = null, bool isUserLibrarian = false);
        Task<IPagedResult<List<LibrarianUserCheckoutDto>>> GetLibrarianUserCheckoutsAsync(BookCheckoutStatusEnum status, int pageNumber, int pageSize = 10);
        Task<string> ChangeCheckoutStatusAsync(int checkoutId, BookCheckoutStatusEnum status);
        Task CheckForDelayedCheckouts();
    }
}