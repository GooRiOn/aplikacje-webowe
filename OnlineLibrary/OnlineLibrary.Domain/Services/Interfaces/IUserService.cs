﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using OnlineLibrary.Infrastructure.DataFlow;
using OnlineLibrary.Infrastructure.Dtos;
using OnlineLibrary.Infrastructure.Entities;

namespace OnlineLibrary.Domain.Services.Interfaces
{
    public interface IUserService : IEntityService<UserEntity>
    {
        Task<UserEntity> FindByNameAsync(string username);
        Task<UserEntity> FindByIdAsync(string userId);
        Task<ClaimsIdentity> CreateIdentityAsync(UserEntity user, string authenticationType);
        Task<IdentityResult> ChangePasswordAsync(string userId, string currentPassword, string newPassword);
        Task<IdentityResult> CreateAsync(UserEntity user, string password);
        Task<UserEntity> FindByIdentityAsync(IIdentity identity);
        IdentityRole GetRoleByName(string name);
        List<IdentityRole> GetRoles();
        List<IdentityRole> GetUserRoles(UserEntity user);

        Task<IPagedResult<List<AdminUserListDto>>> GetAdminUserListAsync(int pageNumber, int pageSize = 10);
        Task<bool> LockUserAsync(string id);
        Task<bool> UnlockUserAsync(string id);
        Task<bool> RemoveUserAsync(string id);
    }
}