﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OnlineLibrary.Infrastructure.DataFlow;
using OnlineLibrary.Infrastructure.Dtos;
using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Infrastructure.Enums;

namespace OnlineLibrary.Domain.Services.Interfaces
{
    public interface IBookService : IEntityService<BookEntity>
    {
        Task<IPagedResult<List<BookDto>>> GetBooksPageAsync(int pageNumber, BookGenreEnum genre, int pageSize = 10);
        Task<BookDto> GetBookByIdAsync(int id);
        Task<List<BookDto>> GetBooksByTitleAsync(string title);
        Task<bool> UpdateBook(BookDto dto);
        Task<bool> RemoveBookAsync(int id);
        Task AddBookAsync(BookEntity book);
        Task<List<SimilarBookDto>> GetSimilarBookByGenre(BookGenreEnum genre, int bookId);
    }
}