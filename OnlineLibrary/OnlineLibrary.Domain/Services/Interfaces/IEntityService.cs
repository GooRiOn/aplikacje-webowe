﻿using System.Linq;
using System.Threading.Tasks;
using OnlineLibrary.Infrastructure.Entities;

namespace OnlineLibrary.Domain.Services.Interfaces
{
    public interface IEntityService<TEntity> where TEntity : class, IInternalEntity
    {
        IQueryable<TEntity> Get();
        TEntity Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Commit();
        Task CommitAsync();
    }
}