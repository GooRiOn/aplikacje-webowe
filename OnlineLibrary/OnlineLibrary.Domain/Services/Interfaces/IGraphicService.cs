﻿using System.Drawing;
using System.Threading.Tasks;
using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Infrastructure.Enums;

namespace OnlineLibrary.Domain.Services.Interfaces
{
    public interface IGraphicService: IEntityService<GraphicEntity>
    {
        Task<GraphicEntity> GetGraphicByIdAsync(int id);
        Task<GraphicEntity> AddGraphicAsync(Image graphic, string fileName, GraphicSizeEnum sizeKind);

    }
}