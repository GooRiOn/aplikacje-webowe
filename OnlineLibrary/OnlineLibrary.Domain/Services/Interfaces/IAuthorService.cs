﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OnlineLibrary.Infrastructure.Dtos;
using OnlineLibrary.Infrastructure.Entities;

namespace OnlineLibrary.Domain.Services.Interfaces
{
    public interface IAuthorService : IEntityService<AuthorEntity>
    {
        Task<List<AuthorDto>> GetAuthorsAsync();
    }
}