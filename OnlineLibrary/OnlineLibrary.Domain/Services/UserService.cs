﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using OnlineLibrary.Domain.Services.Interfaces;
using OnlineLibrary.Infrastructure.DataFlow;
using OnlineLibrary.Infrastructure.Dtos;
using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Repository.Auth.Interfaces;
using OnlineLibrary.Repository.Repo.Interfaces;

namespace OnlineLibrary.Domain.Services
{
    public class UserService : EntityService<UserEntity,IUserRepo>, IUserService
    {
        private readonly IUserAccountManager _userAccountManager;
        private readonly IRoleManager _roleManager;

        public UserService(IUserRepo repo,IUserAccountManager userAccountManager, IRoleManager roleManager)
            :base(repo)
        {
            _userAccountManager = userAccountManager;
            _roleManager = roleManager;
        }

        public async Task<UserEntity> FindByNameAsync(string username)
        {
            return await _userAccountManager.FindByNameAsync(username);
        }

        public async Task<UserEntity> FindByIdAsync(string userId)
        {
            return await Repo.Query.FirstOrDefaultAsync(u => u.Id == userId);
        }

        public async Task<ClaimsIdentity> CreateIdentityAsync(UserEntity user, string authenticationType)
        {
            return await _userAccountManager.CreateIdentityAsync(user, authenticationType);
        }

        public async Task<IdentityResult> ChangePasswordAsync(string userId, string currentPassword, string newPassword)
        {
            return await _userAccountManager.ChangePasswordAsync(userId, currentPassword, newPassword);
        }

        public async Task<IdentityResult> CreateAsync(UserEntity user, string password)
        {
            return await _userAccountManager.CreateAsync(user, password);
        }

        public async Task<UserEntity> FindByIdentityAsync(IIdentity identity)
        {
            return await _userAccountManager.FindByIdentityAsync(identity);
        }

        public List<IdentityRole> GetRoles()
        {
            return _roleManager.GetRoles().ToList();
        }

        public IdentityRole GetRoleByName(string name)
        {
            return _roleManager.GetRoleByName(name);
        }

        public List<IdentityRole> GetUserRoles(UserEntity user)
        {
            var roles = _roleManager.GetRoles().ToList();

            return roles.Where(role => user.Roles.Any(r => r.RoleId == role.Id)).ToList();
        }

        public async Task<IPagedResult<List<AdminUserListDto>>> GetAdminUserListAsync(int pageNumber, int pageSize = 10)
        {
            var skippedDataSize = (pageNumber - 1) * pageSize;
            var totalPagesNumber = (int)Math.Ceiling((double)Repo.Query.Count() / pageSize);

            var pageResult = await Repo.Query.Select(u => new AdminUserListDto
            {
                Id = u.Id,
                UserName = u.UserName,
                FirstName = u.FirstName,
                Surname = u.Surname,
                Email = u.Email,
                IsLocked = u.IsLocked
            }).OrderBy(u => u.UserName).Skip(skippedDataSize).Take(pageSize).ToListAsync();

            return new PagedResult<List<AdminUserListDto>>
            {
                Result = pageResult,
                TotalPagesNumber = totalPagesNumber
            };
        }

        public async Task<bool> LockUserAsync(string id)
        {
            var user = Repo.Query.FirstOrDefault(u => u.Id == id);

            if (user == null)
                return false;

            var lockableUser = (ILockable) user;
            lockableUser.Lock();
            await Repo.CommitAsync();

            return true;
        }

        public async Task<bool> UnlockUserAsync(string id)
        {
            var user = Repo.Query.FirstOrDefault(u => u.Id == id);

            if (user == null)
                return false;

            var lockableUser = (ILockable)user;
            lockableUser.Unlock();
            await Repo.CommitAsync();

            return true;
        }

        public async Task<bool> RemoveUserAsync(string id)
        {
            var user = Repo.Query.FirstOrDefault(u => u.Id == id);

            if (user == null)
                return false;

            Repo.Delete(user);
            await Repo.CommitAsync();

            return true;
        }
    }
}
