﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using OnlineLibrary.Domain.Services.Interfaces;
using OnlineLibrary.Infrastructure.DataFlow;
using OnlineLibrary.Infrastructure.Dtos;
using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Infrastructure.Enums;
using OnlineLibrary.Repository.Repo.Interfaces;

namespace OnlineLibrary.Domain.Services
{
    public class BookCheckoutService: EntityService<BookCheckoutEntity, IBookCheckoutRepo>, IBookCheckoutService
    {
        private readonly IBookRepo _bookRepo;

        public BookCheckoutService(IBookCheckoutRepo repo, IBookRepo bookRepo)
            : base(repo)
        {
            _bookRepo = bookRepo;
        }

        public async Task<DelayNotificationDto> GetDelayNotificationDtoAsync(int checkoutId)
        {
            return await Repo.Query.Where(c => c.Id == checkoutId).Select(c => new DelayNotificationDto
            {
                User = c.User,
                DaysDueNumber = DbFunctions.DiffDays(c.ExpirationDate, DateTime.UtcNow).Value
            }).FirstOrDefaultAsync();
        }

        public async Task<bool> CheckoutBookAsync(int bookId, string userId)
        {
            var book = _bookRepo.Query.FirstOrDefault(b => b.Id == bookId);
            var hasSameCheckout = Repo.Query.Any(c => c.IsActive && c.UserId == userId && c.BookId == bookId);

            if (book == null || hasSameCheckout)
                return false;

            var bookCheckout = new BookCheckoutEntity
            {
                BookId = bookId,
                UserId = userId,
            };

            Repo.Add(bookCheckout);

            book.Quantity -= 1;

            await Repo.CommitAsync();

            return true;
        }

        public async Task<List<UserCheckoutsDto>> GetUserCheckoutsAsync(string userId)
        {
            return await Repo.Query.Where(c => c.IsActive && c.UserId == userId).Select(c => new UserCheckoutsDto
            {
                Id = c.Id,
                BookTitle = c.Book.Title,
                CheckoutDate = c.CreatedDate,
                ExpirationDate = c.ExpirationDate,
                Status = c.Status
            }).ToListAsync();
        }

        public async Task<bool> RemoveCheckoutAsync(int checkoutId, string userId = null, bool isUserLibrarian = false)
        {
            var checkout = await Repo.Query.FirstOrDefaultAsync(c => c.Id == checkoutId);

            if (!isUserLibrarian && ( checkout == null || checkout.UserId != userId))
                return false;

            Repo.Delete(checkout);
            await Repo.CommitAsync();

            return true;
        }

        public async Task<IPagedResult<List<LibrarianUserCheckoutDto>>> GetLibrarianUserCheckoutsAsync(BookCheckoutStatusEnum status, int pageNumber, int pageSize = 10)
        {
            var qBookCheckouts = Repo.Query.Where(c => c.IsActive && c.Status == status);


            var skippedDataSize = (pageNumber - 1) * pageSize;
            var totalPagesNumber = (int)Math.Ceiling((double)qBookCheckouts.Count() / pageSize);

            var pageResult = await qBookCheckouts.Select(c => new LibrarianUserCheckoutDto
            {
                Id = c.Id,
                Username = c.User.UserName,
                BookTitle = c.Book.Title,
                CheckoutDate = c.CreatedDate,
                ExpirationDate = c.ExpirationDate,
                Status = c.Status
            }).OrderBy(c => c.CheckoutDate).Skip(skippedDataSize).Take(10).ToListAsync();

            return new PagedResult<List<LibrarianUserCheckoutDto>>
            {
                Result = pageResult,
                TotalPagesNumber = totalPagesNumber
            };
        }

        public async Task<string> ChangeCheckoutStatusAsync(int checkoutId, BookCheckoutStatusEnum status)
        {
            var checkout = await Repo.Query.FirstOrDefaultAsync(c => c.Id == checkoutId);

            if (checkout == null)
                return string.Empty;

            checkout.Status = status;

            Repo.Update(checkout);
            await Repo.CommitAsync();

            return checkout.UserId;
        }

        public async Task CheckForDelayedCheckouts()
        {
            var delayedCheckouts = await Repo.Query.Where(c => c.IsActive && c.Status == BookCheckoutStatusEnum.Active && c.ExpirationDate < DateTime.UtcNow ).ToListAsync();

            foreach (var checkout in delayedCheckouts)
            {
                checkout.Status = BookCheckoutStatusEnum.Delayed;
                Repo.Update(checkout);
            }

            await Repo.CommitAsync();
        }
    }
}