﻿using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.SignalR;
using OnlineLibrary.Domain.Services.Interfaces;
using OnlineLibrary.Hubs;
using OnlineLibrary.Infrastructure.Enums;
using OnlineLibrary.Providers;

namespace OnlineLibrary.Controllers
{
    [RoutePrefix("api"), System.Web.Http.Authorize]
    public class BookController : BaseApiController
    {
        private readonly IUserService _userService;
        private readonly IBookService _bookService;
        private readonly IBookCheckoutService _bookCheckoutService;

        public BookController(IUserService userService, IBookService bookService, IBookCheckoutService bookCheckoutService) 
            : base(userService)
        {
            _userService = userService;
            _bookService = bookService;
            _bookCheckoutService = bookCheckoutService;
        }


        [HttpGet, Route("Books/{genre}/Genre/{pageNumber}/Page"),AllowAnonymous]
        public async Task<IHttpActionResult> GetBookPageAsync(BookGenreEnum genre, int pageNumber)
        {
            var result = await _bookService.GetBooksPageAsync(pageNumber, genre);

            return Ok(result);
        }

        [HttpGet, Route("Book/{id}"), AllowAnonymous]
        public async Task<IHttpActionResult> GetBookByIdAsync(int id)
        {
            var book = await _bookService.GetBookByIdAsync(id);

            if (book == null)
                return BadRequest();

            var similarBooks = await _bookService.GetSimilarBookByGenre(book.Genre, book.Id);

            return Ok(new {Book = book, SimilarBooks = similarBooks});
        }

        [HttpGet, Route("Books/{title}"), AllowAnonymous]
        public async Task<IHttpActionResult> GetBooksByIdAsync(string title)
        {
            var result = await _bookService.GetBooksByTitleAsync(title);

            if (result == null)
                return BadRequest();

            return Ok(result);
        }

        [HttpPost,Route("Book/{bookId}/Checkout")]
        public async Task<IHttpActionResult> CheckoutBookAsync(int bookId)
        {
            var user = await base.GetUserAsync();

            var result = await _bookCheckoutService.CheckoutBookAsync(bookId, user.Id);

            if (!result)
                return BadRequest();

            var emailSubject = "Your order has been placed !";
            var emailBody = "We will inform you about book checkout status.";

            MailProvider.SendMail(emailSubject,emailBody,user.Email);

            return Ok();
        }

        [HttpPost, Route("Checkouts/Delayed/Check"),AllowAnonymous]
        public async Task<IHttpActionResult> CheckForDelayedCheckouts()
        {
            await Task.Run(async () =>
            {
                await _bookCheckoutService.CheckForDelayedCheckouts();
            });

            return Ok();
        }
    }
}
