﻿using System.Threading.Tasks;
using System.Web.Http;
using OnlineLibrary.Domain.Services.Interfaces;
using OnlineLibrary.Infrastructure.Helpers;
using OnlineLibrary.Results;

namespace OnlineLibrary.Controllers
{
    [RoutePrefix("api")]
    public class GraphicController : ApiController
    {
        private readonly IGraphicService _graphicService;

        public GraphicController(IGraphicService graphicService)
        {
            _graphicService = graphicService;
        }


        [Route("Graphic/{graphicId}"), AllowAnonymous]
        public async Task<IHttpActionResult> GetGraphicByIdAsync(int graphicId)
        {
            var graphicOrig = await _graphicService.GetGraphicByIdAsync(graphicId);

            if (graphicOrig == null)
                return NotFound();

            return BinaryData(graphicOrig.Content, GraphicHelper.GetContentType(graphicOrig.FileName));
        }

        protected BinaryDataResult BinaryData(byte[] data, string contentType)
        {
            return new BinaryDataResult(data, contentType);
        }
    }
}
