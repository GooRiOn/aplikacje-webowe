﻿using System.Threading.Tasks;
using System.Web.Http;
using OnlineLibrary.Domain.Services.Interfaces;
using OnlineLibrary.Infrastructure.Entities;

namespace OnlineLibrary.Controllers
{
    public class BaseApiController : ApiController
    {
        protected readonly IUserService UserService;

        public BaseApiController(IUserService userService)
        {
            UserService = userService;
        }

        public async Task<UserEntity> GetUserAsync()
        {
            return await UserService.FindByIdentityAsync(User.Identity);
        }

        public async Task<string> GetUserIdAsync()
        {
            var user = await UserService.FindByIdentityAsync(User.Identity);
            return user == null ? string.Empty : user.Id;
        }
    }
}
