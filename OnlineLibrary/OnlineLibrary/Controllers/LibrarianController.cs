﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using OnlineLibrary.Auth;
using OnlineLibrary.Domain.Services.Interfaces;
using OnlineLibrary.Infrastructure.DataFlow;
using OnlineLibrary.Infrastructure.Dtos;
using OnlineLibrary.Infrastructure.Entities;
using OnlineLibrary.Infrastructure.Enums;
using OnlineLibrary.Providers;

namespace OnlineLibrary.Controllers
{
   [RoutePrefix("api/Librarian"), Authorize,OnlyLibrarian]
    public class LibrarianController : BaseApiController
   {

       private readonly IBookService _bookService;
       private readonly IBookCheckoutService _bookCheckoutService;
       private readonly IGraphicService _graphicService;
       private readonly IUserService _userService;

        public LibrarianController(IUserService userService, IBookService bookService, IGraphicService graphicService, IBookCheckoutService bookCheckoutService)
            :base(userService)
        {
            _userService = userService;
            _bookService = bookService;
            _graphicService = graphicService;
            _bookCheckoutService = bookCheckoutService;
        }

       [HttpPost, Route("Book/Update")]
       public async Task<IHttpActionResult> UpdateBookAsync(BookDto dto)
       {
           var result = await _bookService.UpdateBook(dto);

           if (!result)
               return BadRequest();

           return Ok();
       }

       [HttpPost, Route("Book/{id}/Remove")]
       public async Task<IHttpActionResult> RemoveBookAsync(int id)
       {
           var result = await _bookService.RemoveBookAsync(id);

           if (!result)
               return BadRequest();

           return Ok();
       }

       [HttpPost, Route("Book/Add")]
       public async Task<IHttpActionResult> AddBookAsync(BookEntity book)
       {
           await _bookService.AddBookAsync(book);

           return Ok();
       }

       [HttpPost, Route("Book/Cover/Upload")]
       public async Task<IHttpActionResult> UploadBookCoverAsync()
       {
           var httpRequest = HttpContext.Current.Request;
           var graphicEntity = new GraphicEntity();

           if (httpRequest.Files.Count > 0)
           {
               foreach (string file in httpRequest.Files)
               {
                   var postedFile = httpRequest.Files[file];
                   var graphic = new Bitmap(postedFile.InputStream);
                   graphicEntity = await _graphicService.AddGraphicAsync(graphic,postedFile.FileName,GraphicSizeEnum.BookFrontCover);
                   break;
               }
           }

           return Ok(graphicEntity.Id);
       }

       [HttpGet,Route("Users/Checkouts/{status}/Status/{pageNumber}/Page")]
       public async Task<OkNegotiatedContentResult<IPagedResult<List<LibrarianUserCheckoutDto>>>> GetUserCheckoutsAsync(BookCheckoutStatusEnum status, int pageNumber)
       {
           var result = await _bookCheckoutService.GetLibrarianUserCheckoutsAsync(status, pageNumber);
           return Ok(result);
       }

       [HttpPost,Route("Checkout/{checkoutId}/{status}/Status/Update")]
       public async Task<IHttpActionResult> ChangeCheckoutStatusAsync(int checkoutId, BookCheckoutStatusEnum status)
       {
           var userId = await _bookCheckoutService.ChangeCheckoutStatusAsync(checkoutId, status);

           if (String.IsNullOrEmpty(userId))
               return BadRequest();

           var emailSubject = "Your checkout status has been changed !";
           var emailBody = "Your checkout is now:" + status;

           var user = await _userService.FindByIdAsync(userId);

           MailProvider.SendMail(emailSubject, emailBody, user.Email);

           return Ok();
       }

       [HttpPost, Route("Checkout/{checkoutId}/Remove")]
       public async Task<IHttpActionResult> DeleteCheckoutAsync(int checkoutId)
       {
           var result = await _bookCheckoutService.RemoveCheckoutAsync(checkoutId,null, true);

           if (!result)
               return BadRequest();

           return Ok();
       }

       [HttpPost, Route("Checkout/{checkoutId}/Notification/Send")]
       public async Task<IHttpActionResult> SendNotificationAsync(int checkoutId)
       {
           var delayNotificationDto = await _bookCheckoutService.GetDelayNotificationDtoAsync(checkoutId);

           var emailSubject = "Your checkout time has expired !";
           var emailBody = "The day to return your book has passed. Please return it ASAP. For each day of delay, you will be charged $0.30. Your current fee is: $" + delayNotificationDto.DaysDueNumber * 0.3 +".";

           MailProvider.SendMail(emailSubject, emailBody, delayNotificationDto.User.Email);

           return Ok();

       }
    }
}
