﻿using System.Threading.Tasks;
using System.Web.Http;
using OnlineLibrary.Auth;
using OnlineLibrary.Domain.Services.Interfaces;

namespace OnlineLibrary.Controllers
{
    [Authorize,RoutePrefix("api/Admin"),OnlyAdmin]
    public class AdminController : BaseApiController
    {
        public AdminController(IUserService userService) : base(userService)
        {
        }

        [HttpGet,Route("Users/List/{pageNumber}/Page")]
        public async Task<IHttpActionResult> GetAdminUserListAsync(int pageNumber)
        {
            var result = await UserService.GetAdminUserListAsync(pageNumber);

            return Ok(result);
        }

        [HttpPost, Route("User/{id}/Lock")]
        public async Task<IHttpActionResult> LockUserAsync(string id)
        {
            var result = await UserService.LockUserAsync(id);

            if(!result)
                return BadRequest();
            
            return Ok();
        }

        [HttpPost, Route("User/{id}/Unlock")]
        public async Task<IHttpActionResult> UnlockUserAsync(string id)
        {
            var result = await UserService.UnlockUserAsync(id);

            if (!result)
                return BadRequest();

            return Ok();
        }

        [HttpPost, Route("User/{id}/Remove")]
        public async Task<IHttpActionResult> RemoveUserAsync(string id)
        {
            var result = await UserService.RemoveUserAsync(id);

            if (!result)
                return BadRequest();

            return Ok();
        }
    }
}
