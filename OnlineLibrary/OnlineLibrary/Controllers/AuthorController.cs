﻿using System.Threading.Tasks;
using System.Web.Http;
using OnlineLibrary.Domain.Services.Interfaces;

namespace OnlineLibrary.Controllers
{
    [RoutePrefix("api")]
    public class AuthorController : BaseApiController
    {
        private readonly IUserService _userService;
        private readonly IAuthorService _authorService;

        public AuthorController(IUserService userService, IAuthorService authorService)
            :base(userService)
        {
            _userService = userService;
            _authorService = authorService;
        }


        [HttpGet, Route("Authors")]
        public async Task<IHttpActionResult> GetAuthorsAsync()
        {
            var result = await _authorService.GetAuthorsAsync();
            return Ok(result);
        }
    }
}
