﻿using System.Threading.Tasks;
using System.Web.Http;
using OnlineLibrary.Domain.Services.Interfaces;

namespace OnlineLibrary.Controllers
{
    [RoutePrefix("api/Me")]
    public class UserController : BaseApiController
    {

        private readonly IUserService _userService;
        private readonly IBookCheckoutService _bookCheckoutService;

        public UserController(IUserService userService, IBookCheckoutService bookCheckoutService)
            :base(userService)
        {
            _userService = userService;
            _bookCheckoutService = bookCheckoutService;
        }

        [HttpGet, Route("Checkouts")]
        public async Task<IHttpActionResult> GetUserCheckoutsAsync()
        {
            var userId = await base.GetUserIdAsync();

            var result = await _bookCheckoutService.GetUserCheckoutsAsync(userId);

            return Ok(result);
        }

        [HttpPost, Route("Checkout/{checkoutId}/Remove")]
        public async Task<IHttpActionResult> RemoveUserCheckoutAsync(int checkoutId)
        {
            var userId = await base.GetUserIdAsync();

            var result = await _bookCheckoutService.RemoveCheckoutAsync(checkoutId, userId);

            if (!result)
                return BadRequest();

            return Ok();
        }
    }
}
