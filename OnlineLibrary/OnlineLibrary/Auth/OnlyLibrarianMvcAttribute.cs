﻿using System.Web;
using System.Web.Mvc;
using OnlineLibrary.Infrastructure.Statics;

namespace OnlineLibrary.Auth
{
    internal class OnlyLibrarianMvcAttribute : AuthorizeAttribute
    {
        public OnlyLibrarianMvcAttribute()
        {
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return httpContext.User.IsInRole(SystemRoles.Librarian);
        }
    }
}
