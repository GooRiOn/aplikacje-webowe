﻿using System.Web;
using System.Web.Mvc;
using OnlineLibrary.Infrastructure.Statics;

namespace OnlineLibrary.Auth
{
    class OnlyAdminMvcAttribute : AuthorizeAttribute
    {
        public OnlyAdminMvcAttribute()
        {
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return httpContext.User.IsInRole(SystemRoles.Admin);
        }
    }
}
