﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using OnlineLibrary.Infrastructure.Statics;


namespace OnlineLibrary.Auth
{
    class OnlyLibrarianAttribute : AuthorizeAttribute
    {
        public OnlyLibrarianAttribute()
        {
        }

        public override bool AllowMultiple { get { return false; } }

        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var user = actionContext.RequestContext.Principal;

            if (user.IsInRole(SystemRoles.Librarian))
            {
                base.OnAuthorization(actionContext);
                return;
            }

            var response = new HttpResponseMessage(HttpStatusCode.Unauthorized);

            actionContext.Response = response;
        }
    }
}
