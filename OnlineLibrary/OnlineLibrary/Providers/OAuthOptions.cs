﻿using System;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;

namespace OnlineLibrary.Providers
{
    public class OAuthOptions : OAuthAuthorizationServerOptions
    {
        public readonly string PublicClientId = "self";

        public OAuthOptions()
        {
            TokenEndpointPath = new PathString("/Token");
            AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin");
            AccessTokenExpireTimeSpan = TimeSpan.FromDays(14);
            AllowInsecureHttp = true;
            ApplicationCanDisplayErrors = true;
        }


    }
}
