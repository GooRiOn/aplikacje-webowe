﻿using System.Net.Mail;

namespace OnlineLibrary.Providers
{
    public static class MailProvider
    {
        public static void SendMail(string subject, string body, string receiverMail)
        {
            try
            {
                var client = new SmtpClient();

                var mail = new MailMessage
                {
                    IsBodyHtml = false,
                    Subject = subject,
                    Body = body
                };

                mail.To.Add(new MailAddress(receiverMail));

                client.Send(mail);
            }
            catch { }
        }
    }
}
