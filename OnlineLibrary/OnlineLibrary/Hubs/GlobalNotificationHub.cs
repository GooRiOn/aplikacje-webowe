﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;

namespace OnlineLibrary.Hubs
{
    public class GlobalNotificationHub : Hub
    {
        public void SendGlobalNotification(string message)
        {
            Clients.All.sendGlobalNotification(message);
        }

    }
}
