/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../../../../scripts/app/dataflow.ts" />
/// <reference path="../../../../scripts/typings/signalr/signalr.d.ts" />
var User;
(function (User) {
    var BookInfoController = (function () {
        function BookInfoController(service, $scope, $stateParams) {
            this.service = service;
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.bookGenres = [
                { value: "1", name: "Fantasy" },
                { value: "2", name: "Thriller" },
                { value: "3", name: "Drama" },
                { value: "4", name: "Comedy" },
                { value: "5", name: "All" },
            ];
            this.isLoading = false;
            this.bookId = this.$stateParams.id;
            this.getBookById();
        }
        BookInfoController.prototype.getBookById = function () {
            var self = this;
            this.service.getBookById(self.bookId).success(function (result) {
                self.book = result.book;
                self.similarBooks = result.similarBooks;
            });
        };
        BookInfoController.prototype.checkoutBook = function () {
            var self = this;
            if (!App.Auth.getInstance().user || !App.Auth.getInstance().user.isLoggedIn) {
                toastr.warning("You must be logged in to proceed");
                return;
            }
            self.isLoading = true;
            this.service.checkoutBook(this.bookId).success(function () {
                self.book.quantity -= 1;
                self.isLoading = false;
                toastr.success("Request sent. You will be informed about your request's status by email");
            }).error(function () {
                self.isLoading = false;
                toastr.warning("This book has been already checked out");
            });
        };
        BookInfoController.prototype.getBookGenreNameByValue = function (numericValue) {
            if (!numericValue)
                return;
            var value = numericValue.toString();
            return this.bookGenres.firstOrNull(function (b) { return b.value === value; }).name;
        };
        BookInfoController.$inject = ["bookInfoFactory", "$scope", '$stateParams'];
        return BookInfoController;
    })();
    User.BookInfoController = BookInfoController;
    angular.module("bookInfo", []).controller("bookInfoCtrl", User.BookInfoController);
})(User || (User = {}));
//# sourceMappingURL=bookInfoController.js.map