﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../../../../scripts/app/dataflow.ts" />

module User {
    export class BookCatalogController {

        static $inject = ["bookCatalogFactory", "$scope"]

        pageNumber = 1
        totalPagesNumber = 1
        booksPage: BookDto[]

        genreSelection = {

            selectedGenre: { value: "5", name: "All" },
            bookGenres:
            [
                { value: "1", name: "Fantasy" },
                { value: "2", name: "Thriller" },
                { value: "3", name: "Drama" },
                { value: "4", name: "Comedy" },
                { value: "5", name: "All" },
            ]
        }

        selectedBookId: number

        wantedBookTitle =""
        isSearchByTitleActive = false

        constructor(private service: IBookCatalogService, private $scope: ng.IScope) {
            this.subscribeProperties()
        }

        subscribeProperties() {
            var self = this

            self.$scope.$watch(() => self.pageNumber, (newValue: number, oldValue: number) => {

                if (newValue <= 1)
                    self.pageNumber = 1


                if (newValue >= self.totalPagesNumber)
                    self.pageNumber = self.totalPagesNumber


                self.getBookList()
            })

            self.$scope.$watch(() => self.genreSelection.selectedGenre, (newValue: any, oldValue: any) => {
                self.getBookList()
            })

            self.$scope.$watch(() => self.wantedBookTitle, (newValue: string, oldValue: string) => {

                if(newValue && newValue.length > 3)
                    self.getBookListByTitle()

                else if (newValue.length === 0) {
                    self.isSearchByTitleActive = false
                    self.getBookList()
                }
                   
            })
        }

        getBookList() {
            var self = this

            var genreNumericValue = parseInt(this.genreSelection.selectedGenre.value)

            this.service.getBookList(genreNumericValue, this.pageNumber).success((bookPageResult: App.IPagedResult<BookDto[]>) => {
               
                self.booksPage = bookPageResult.result
                self.totalPagesNumber = bookPageResult.totalPagesNumber
            })
        }

        getBookListByTitle() {
            
            var self = this

            self.service.getBookListByTitle(self.wantedBookTitle).success((result: BookDto[]) => {
                self.isSearchByTitleActive = true
                self.booksPage = result
                self.totalPagesNumber = 1
            })
        }

        selectBook(id: number) {
            this.selectedBookId = id
        }

        getBookGenreNameByValue(numericValue: number): string {

            var value = numericValue.toString()

            return this.genreSelection.bookGenres.firstOrNull(b => b.value === value).name
        }
    }

    angular.module("bookCatalog", []).controller("bookCatalogCtrl", User.BookCatalogController);
}    