/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../../../../scripts/app/dataflow.ts" />
var User;
(function (User) {
    var BookCatalogController = (function () {
        function BookCatalogController(service, $scope) {
            this.service = service;
            this.$scope = $scope;
            this.pageNumber = 1;
            this.totalPagesNumber = 1;
            this.genreSelection = {
                selectedGenre: { value: "5", name: "All" },
                bookGenres: [
                    { value: "1", name: "Fantasy" },
                    { value: "2", name: "Thriller" },
                    { value: "3", name: "Drama" },
                    { value: "4", name: "Comedy" },
                    { value: "5", name: "All" },
                ]
            };
            this.wantedBookTitle = "";
            this.isSearchByTitleActive = false;
            this.subscribeProperties();
        }
        BookCatalogController.prototype.subscribeProperties = function () {
            var self = this;
            self.$scope.$watch(function () { return self.pageNumber; }, function (newValue, oldValue) {
                if (newValue <= 1)
                    self.pageNumber = 1;
                if (newValue >= self.totalPagesNumber)
                    self.pageNumber = self.totalPagesNumber;
                self.getBookList();
            });
            self.$scope.$watch(function () { return self.genreSelection.selectedGenre; }, function (newValue, oldValue) {
                self.getBookList();
            });
            self.$scope.$watch(function () { return self.wantedBookTitle; }, function (newValue, oldValue) {
                if (newValue && newValue.length > 3)
                    self.getBookListByTitle();
                else if (newValue.length === 0) {
                    self.isSearchByTitleActive = false;
                    self.getBookList();
                }
            });
        };
        BookCatalogController.prototype.getBookList = function () {
            var self = this;
            var genreNumericValue = parseInt(this.genreSelection.selectedGenre.value);
            this.service.getBookList(genreNumericValue, this.pageNumber).success(function (bookPageResult) {
                self.booksPage = bookPageResult.result;
                self.totalPagesNumber = bookPageResult.totalPagesNumber;
            });
        };
        BookCatalogController.prototype.getBookListByTitle = function () {
            var self = this;
            self.service.getBookListByTitle(self.wantedBookTitle).success(function (result) {
                self.isSearchByTitleActive = true;
                self.booksPage = result;
                self.totalPagesNumber = 1;
            });
        };
        BookCatalogController.prototype.selectBook = function (id) {
            this.selectedBookId = id;
        };
        BookCatalogController.prototype.getBookGenreNameByValue = function (numericValue) {
            var value = numericValue.toString();
            return this.genreSelection.bookGenres.firstOrNull(function (b) { return b.value === value; }).name;
        };
        BookCatalogController.$inject = ["bookCatalogFactory", "$scope"];
        return BookCatalogController;
    })();
    User.BookCatalogController = BookCatalogController;
    angular.module("bookCatalog", []).controller("bookCatalogCtrl", User.BookCatalogController);
})(User || (User = {}));
//# sourceMappingURL=bookCatalogController.js.map