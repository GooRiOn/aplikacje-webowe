﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../../../../scripts/app/dataflow.ts" />
/// <reference path="../../../../scripts/typings/signalr/signalr.d.ts" />

module User {
    export class BookInfoController {

        static $inject = ["bookInfoFactory", "$scope", '$stateParams']

        scope: ng.IScope
        bookId: number
        book: BookDto
        similarBooks: SimilarBookDto[]

        bookGenres =
        [
            { value: "1", name: "Fantasy" },
            { value: "2", name: "Thriller" },
            { value: "3", name: "Drama" },
            { value: "4", name: "Comedy" },
            { value: "5", name: "All" },
        ]

        isLoading = false
        
        constructor(private service: IBookInfoService, private $scope: ng.IScope, private $stateParams : any) {
            
            this.bookId = this.$stateParams.id
            this.getBookById()
        }
        
        getBookById() {
            
            var self = this

            this.service.getBookById(self.bookId).success((result: BookInfoDto) => {
                self.book = result.book
                self.similarBooks = result.similarBooks
            })
        }

        checkoutBook() {

            var self = this
            
            if (!App.Auth.getInstance().user || !App.Auth.getInstance().user.isLoggedIn) {
                toastr.warning("You must be logged in to proceed")
                return
            }
            
            self.isLoading = true

            this.service.checkoutBook(this.bookId).success(() => {
                
                self.book.quantity -= 1
                self.isLoading = false
                toastr.success("Request sent. You will be informed about your request's status by email")

            }).error(() => {
                self.isLoading = false
                toastr.warning("This book has been already checked out")
            })

        }

        getBookGenreNameByValue(numericValue: number): string {

            if (!numericValue)
                return

            var value = numericValue.toString()

            return this.bookGenres.firstOrNull(b => b.value === value).name
        }
    }

    angular.module("bookInfo", []).controller("bookInfoCtrl", User.BookInfoController);
}     