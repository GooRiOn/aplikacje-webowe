/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../../../../scripts/app/dataflow.ts" />
var User;
(function (User) {
    var MyCheckoutsController = (function () {
        function MyCheckoutsController(service, $scope) {
            this.service = service;
            this.$scope = $scope;
            this.checkoutStatuses = [
                { value: "1", name: "Request sent" },
                { value: "2", name: "Awaiting" },
                { value: "3", name: "Active" },
                { value: "4", name: "Delayed" },
            ];
            this.getMyCheckouts();
        }
        MyCheckoutsController.prototype.getMyCheckouts = function () {
            var self = this;
            self.service.getMyCheckouts().success(function (result) {
                self.checkouts = result;
            });
        };
        MyCheckoutsController.prototype.getStatusNameByValue = function (numericValue) {
            if (!numericValue)
                return;
            var value = numericValue.toString();
            return this.checkoutStatuses.firstOrNull(function (c) { return c.value === value; }).name;
        };
        MyCheckoutsController.prototype.selectCheckout = function (checkoutId) {
            this.selectedCheckoutId = checkoutId;
        };
        MyCheckoutsController.prototype.removeCheckout = function (checkout) {
            var self = this;
            var confirm = window.confirm("Are you sure you want to remove this checkout?");
            if (!confirm)
                return;
            self.service.removeCheckout(checkout.id).success(function () {
                var index = self.checkouts.indexOf(checkout);
                self.checkouts.splice(index, 1);
                toastr.success("Checkout has been removed successfully");
            });
        };
        MyCheckoutsController.$inject = ["myCheckoutsFactory", "$scope"];
        return MyCheckoutsController;
    })();
    User.MyCheckoutsController = MyCheckoutsController;
    angular.module("myCheckouts", []).controller("myCheckoutsCtrl", User.MyCheckoutsController);
})(User || (User = {}));
//# sourceMappingURL=myCheckoutsController.js.map