﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../../../../scripts/app/dataflow.ts" />

module User {
    export class MyCheckoutsController {

        static $inject = ["myCheckoutsFactory", "$scope"]

        checkouts: MyCheckoutDto[]

        checkoutStatuses =
        [
            { value: "1", name: "Request sent" },
            { value: "2", name: "Awaiting" },
            { value: "3", name: "Active" },
            { value: "4", name: "Delayed" },
         ]


        selectedCheckoutId: number

        constructor(private service: IMyCheckoutsService, private $scope: ng.IScope) {
           this.getMyCheckouts()
        }

        getMyCheckouts() {
            var self = this

            self.service.getMyCheckouts().success((result: MyCheckoutDto[]) => {
                self.checkouts = result
            })
        }

        getStatusNameByValue(numericValue: number): string {
            if (!numericValue)
                return

            var value = numericValue.toString()

            return this.checkoutStatuses.firstOrNull(c => c.value === value).name
        }

        selectCheckout(checkoutId: number) {
            this.selectedCheckoutId = checkoutId
        }

        removeCheckout(checkout: MyCheckoutDto) {
            var self = this

            var confirm = window.confirm("Are you sure you want to remove this checkout?")

            if (!confirm)
                return

            self.service.removeCheckout(checkout.id).success(() => {
                var index = self.checkouts.indexOf(checkout)
                self.checkouts.splice(index, 1)

                toastr.success("Checkout has been removed successfully")
            })
        }
        
    }

    angular.module("myCheckouts", []).controller("myCheckoutsCtrl", User.MyCheckoutsController);
}    