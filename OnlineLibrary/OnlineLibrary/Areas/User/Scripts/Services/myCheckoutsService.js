/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../scripts/app/dataflow.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var User;
(function (User) {
    var MyCheckoutsService = (function (_super) {
        __extends(MyCheckoutsService, _super);
        function MyCheckoutsService($http) {
            _super.call(this, $http);
            this.$http = $http;
        }
        MyCheckoutsService.prototype.getMyCheckouts = function () {
            return _super.prototype.getJSON.call(this, "api/Me/Checkouts", true);
        };
        MyCheckoutsService.prototype.removeCheckout = function (checkoutId) {
            var url = "api/Me/Checkout/" + checkoutId + "/Remove";
            return _super.prototype.postJSON.call(this, url, null, true);
        };
        MyCheckoutsService.$inject = ["$http"];
        return MyCheckoutsService;
    })(App.DataService);
    User.MyCheckoutsService = MyCheckoutsService;
    factory.$inject = ["$http"];
    function factory($http) {
        return new MyCheckoutsService($http);
    }
    angular.module("myCheckoutsService", []).factory("myCheckoutsFactory", factory);
})(User || (User = {}));
//# sourceMappingURL=myCheckoutsService.js.map