/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../scripts/app/dataflow.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var User;
(function (User) {
    var BookCatalogService = (function (_super) {
        __extends(BookCatalogService, _super);
        function BookCatalogService($http) {
            _super.call(this, $http);
            this.$http = $http;
        }
        BookCatalogService.prototype.getBookList = function (genre, pageNumber) {
            var url = "api/Books/" + genre + "/Genre/" + pageNumber + "/Page";
            return _super.prototype.getJSON.call(this, url, true);
        };
        BookCatalogService.prototype.getBookListByTitle = function (title) {
            var url = "api/Books/" + title;
            return _super.prototype.getJSON.call(this, url, false);
        };
        BookCatalogService.$inject = ["$http"];
        return BookCatalogService;
    })(App.DataService);
    User.BookCatalogService = BookCatalogService;
    factory.$inject = ["$http"];
    function factory($http) {
        return new BookCatalogService($http);
    }
    angular.module("bookCatalogService", []).factory("bookCatalogFactory", factory);
})(User || (User = {}));
//# sourceMappingURL=bookCatalogService.js.map