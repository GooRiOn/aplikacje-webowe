﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../scripts/app/dataflow.ts" />

module User {

    export interface IBookCatalogService {
        getBookList(genre: BookGenreEnum, pageNumber: number): ng.IHttpPromise<App.IPagedResult<BookDto[]>>
        getBookListByTitle(title: string): ng.IHttpPromise<BookDto[]>
    }

    export class BookCatalogService extends App.DataService implements IBookCatalogService {

        static $inject = ["$http"]

        constructor(private $http: ng.IHttpService) {
            super($http)
        }

        getBookList(genre: BookGenreEnum, pageNumber: number): ng.IHttpPromise<App.IPagedResult<BookDto[]>> {

            var url = "api/Books/" + genre + "/Genre/" + pageNumber + "/Page"

            return super.getJSON(url, true)
        }

        getBookListByTitle(title: string): ng.IHttpPromise<BookDto[]> {
            
            var url = "api/Books/" + title

            return super.getJSON(url,false)
        }
    }

    factory.$inject = ["$http"]
    function factory($http: ng.IHttpService) {
        return new BookCatalogService($http)
    }

    angular.module("bookCatalogService", []).factory("bookCatalogFactory", factory)
}
  