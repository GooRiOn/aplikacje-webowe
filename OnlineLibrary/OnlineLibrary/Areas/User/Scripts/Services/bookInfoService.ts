﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../scripts/app/dataflow.ts" />

module User {

    export interface IBookInfoService {
        getBookById(id: number): ng.IHttpPromise<BookInfoDto>
        checkoutBook(id: number): angular.IHttpPromise<any>
    }

    export class BookInfoService extends App.DataService implements IBookInfoService {

        static $inject = ["$http"]

        constructor(private $http: ng.IHttpService) {
            super($http)
        }

        getBookById(id: number): angular.IHttpPromise<BookInfoDto> {
            
            var url = "api/Book/" + id

            return super.getJSON(url,false)
        }

        checkoutBook(id: number): angular.IHttpPromise<any> {
            
            var url = "api/Book/" + id + "/Checkout"

            return super.postJSON(url,null,true)
        }
    }

    factory.$inject = ["$http"]
    function factory($http: ng.IHttpService) {
        return new BookInfoService($http)
    }

    angular.module("bookInfoService", []).factory("bookInfoFactory", factory)
}
   