﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../scripts/app/dataflow.ts" />

module User {

    export interface IMyCheckoutsService {
        getMyCheckouts(): ng.IHttpPromise<MyCheckoutDto[]>
        removeCheckout(checkoutId: number): ng.IHttpPromise<any>
    }

    export class MyCheckoutsService extends App.DataService implements IMyCheckoutsService {

        static $inject = ["$http"]

        constructor(private $http: ng.IHttpService) {
            super($http)
        }

        getMyCheckouts(): ng.IHttpPromise<MyCheckoutDto[]> {
            return super.getJSON("api/Me/Checkouts", true)
        }

        removeCheckout(checkoutId: number): ng.IHttpPromise<any> {
            var url = "api/Me/Checkout/" + checkoutId + "/Remove"

            return super.postJSON(url, null,true)
        }
       
    }

    factory.$inject = ["$http"]
    function factory($http: ng.IHttpService) {
        return new MyCheckoutsService($http)
    }

    angular.module("myCheckoutsService", []).factory("myCheckoutsFactory", factory)
}
  