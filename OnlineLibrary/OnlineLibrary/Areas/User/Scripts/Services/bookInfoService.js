/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../scripts/app/dataflow.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var User;
(function (User) {
    var BookInfoService = (function (_super) {
        __extends(BookInfoService, _super);
        function BookInfoService($http) {
            _super.call(this, $http);
            this.$http = $http;
        }
        BookInfoService.prototype.getBookById = function (id) {
            var url = "api/Book/" + id;
            return _super.prototype.getJSON.call(this, url, false);
        };
        BookInfoService.prototype.checkoutBook = function (id) {
            var url = "api/Book/" + id + "/Checkout";
            return _super.prototype.postJSON.call(this, url, null, true);
        };
        BookInfoService.$inject = ["$http"];
        return BookInfoService;
    })(App.DataService);
    User.BookInfoService = BookInfoService;
    factory.$inject = ["$http"];
    function factory($http) {
        return new BookInfoService($http);
    }
    angular.module("bookInfoService", []).factory("bookInfoFactory", factory);
})(User || (User = {}));
//# sourceMappingURL=bookInfoService.js.map