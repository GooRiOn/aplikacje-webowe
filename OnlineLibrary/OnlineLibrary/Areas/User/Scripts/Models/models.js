var User;
(function (User) {
    (function (BookGenreEnum) {
        BookGenreEnum[BookGenreEnum["Fantasy"] = 1] = "Fantasy";
        BookGenreEnum[BookGenreEnum["Thriller"] = 2] = "Thriller";
        BookGenreEnum[BookGenreEnum["Drama"] = 3] = "Drama";
        BookGenreEnum[BookGenreEnum["Commedy"] = 4] = "Commedy";
        BookGenreEnum[BookGenreEnum["All"] = 5] = "All";
    })(User.BookGenreEnum || (User.BookGenreEnum = {}));
    var BookGenreEnum = User.BookGenreEnum;
    (function (CheckoutStatusEnum) {
        CheckoutStatusEnum[CheckoutStatusEnum["requestSent"] = 1] = "requestSent";
        CheckoutStatusEnum[CheckoutStatusEnum["awaiting"] = 2] = "awaiting";
        CheckoutStatusEnum[CheckoutStatusEnum["active"] = 3] = "active";
        CheckoutStatusEnum[CheckoutStatusEnum["delayed"] = 4] = "delayed";
    })(User.CheckoutStatusEnum || (User.CheckoutStatusEnum = {}));
    var CheckoutStatusEnum = User.CheckoutStatusEnum;
    var BookDto = (function () {
        function BookDto() {
        }
        return BookDto;
    })();
    User.BookDto = BookDto;
    var SimilarBookDto = (function () {
        function SimilarBookDto() {
        }
        return SimilarBookDto;
    })();
    User.SimilarBookDto = SimilarBookDto;
    var BookInfoDto = (function () {
        function BookInfoDto() {
        }
        return BookInfoDto;
    })();
    User.BookInfoDto = BookInfoDto;
    var MyCheckoutDto = (function () {
        function MyCheckoutDto() {
        }
        return MyCheckoutDto;
    })();
    User.MyCheckoutDto = MyCheckoutDto;
})(User || (User = {}));
//# sourceMappingURL=models.js.map