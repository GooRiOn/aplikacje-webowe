﻿module User {

    export enum BookGenreEnum {
        Fantasy = 1,
        Thriller = 2,
        Drama = 3,
        Commedy = 4,
        All = 5
    }

    export enum CheckoutStatusEnum {
        requestSent = 1,
        awaiting = 2,
        active = 3,
        delayed = 4
    }

    export class BookDto {

        id: number
        title: string
        genre: BookGenreEnum
        iso: string
        description: string
        quantity: number
        authorId: number
        authorName: string
        authorSurname: string
        graphicId: number
    }

    export class SimilarBookDto {
        id: number
        title: string
        authorNames: string
        graphicId: number
    }

    export class BookInfoDto {
        book: BookDto
        similarBooks: SimilarBookDto[]
    }

    export class MyCheckoutDto {
        id: number
        bookTitle: string
        checkoutDate: Date
        expirationDate: Date
        status: CheckoutStatusEnum
    }
} 