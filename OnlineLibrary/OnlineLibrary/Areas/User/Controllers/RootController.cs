﻿using System.Web.Mvc;

namespace OnlineLibrary.Areas.User.Controllers
{
    public class RootController : Controller
    {
        // GET: User/Root
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BookCatalog()
        {
            return View();
        }

        public ActionResult BookInfo()
        {
            return View();
        }

        public ActionResult MyCheckouts()
        {
            return View();
        }
    }
}