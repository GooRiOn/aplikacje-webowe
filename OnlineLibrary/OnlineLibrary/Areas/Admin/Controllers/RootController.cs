﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineLibrary.Auth;

namespace OnlineLibrary.Areas.Admin.Controllers
{
    [OnlyAdminMvc]
    public class RootController : Controller
    {
        // GET: Admin/Root
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult UserManager()
        {
            return View();
        }

        public ActionResult GlobalNotification()
        {
            return View();
        }

    }
}