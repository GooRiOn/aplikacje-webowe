/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../scripts/app/dataflow.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Admin;
(function (Admin) {
    var UserManagerService = (function (_super) {
        __extends(UserManagerService, _super);
        function UserManagerService($http) {
            _super.call(this, $http);
            this.$http = $http;
        }
        UserManagerService.prototype.getUserList = function (pageNumber) {
            var url = "api/Admin/Users/List/" + pageNumber + "/Page";
            return _super.prototype.getJSON.call(this, url, true);
        };
        UserManagerService.prototype.lockUser = function (id) {
            var url = "api/Admin/User/" + id + "/Lock";
            return _super.prototype.postJSON.call(this, url, null, true);
        };
        UserManagerService.prototype.unlockUser = function (id) {
            var url = "api/Admin/User/" + id + "/Unlock";
            return _super.prototype.postJSON.call(this, url, null, true);
        };
        UserManagerService.prototype.removeUser = function (id) {
            var url = "api/Admin/User/" + id + "/Remove";
            return _super.prototype.postJSON.call(this, url, null, true);
        };
        UserManagerService.prototype.registerLibrarian = function (model) {
            return _super.prototype.postJSON.call(this, "api/Account/Librarian/Register", model, true);
        };
        UserManagerService.$inject = ["$http"];
        return UserManagerService;
    })(App.DataService);
    Admin.UserManagerService = UserManagerService;
    factory.$inject = ["$http"];
    function factory($http) {
        return new UserManagerService($http);
    }
    angular.module("userManagerService", []).factory("userManagerFactory", factory);
})(Admin || (Admin = {}));
//# sourceMappingURL=userManagerService.js.map