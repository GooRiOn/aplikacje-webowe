﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../scripts/app/dataflow.ts" />

module Admin {

    export interface IUserManagerService {
        getUserList(pageNumber: number): ng.IHttpPromise<App.IPagedResult<AdminUserListDto[]>>
        lockUser(id: string): ng.IHttpPromise<any>
        unlockUser(id: string): ng.IHttpPromise<any>
        removeUser(id: string): ng.IHttpPromise<any>
        registerLibrarian(model : LibrarianRegisterModel): ng.IHttpPromise<any>
    }

    export class UserManagerService extends App.DataService implements IUserManagerService {

        static $inject = ["$http"]

        constructor(private $http: ng.IHttpService) {
            super($http)
        }

        getUserList(pageNumber: number): angular.IHttpPromise<App.IPagedResult<AdminUserListDto[]>> {

            var url = "api/Admin/Users/List/" + pageNumber + "/Page"

            return super.getJSON(url,true)
        }

        lockUser(id: string): angular.IHttpPromise<any> {
            
            var url = "api/Admin/User/" + id + "/Lock"

            return super.postJSON(url,null,true)
        }

        unlockUser(id: string): angular.IHttpPromise<any> {
            
            var url = "api/Admin/User/" + id + "/Unlock"

            return super.postJSON(url, null, true)
        }

        removeUser(id: string): angular.IHttpPromise<any> {
            
            var url = "api/Admin/User/" + id + "/Remove"

            return super.postJSON(url, null, true)
        }

        registerLibrarian(model: LibrarianRegisterModel): angular.IHttpPromise<any> {
            
            return super.postJSON("api/Account/Librarian/Register",model,true)
        }
    }

    factory.$inject = ["$http"]
    function factory($http: ng.IHttpService) {
        return new UserManagerService($http)
    }

    angular.module("userManagerService", []).factory("userManagerFactory", factory)
}
  