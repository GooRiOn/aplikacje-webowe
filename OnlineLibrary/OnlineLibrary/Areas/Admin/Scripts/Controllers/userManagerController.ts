﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../../../../scripts/app/dataflow.ts" />

module Admin {
    export class UserManagerController {

        static $inject = ["userManagerFactory", "$scope"];
        
        pageNumber = 1
        totalPagesNumber = 1
        usersPage: AdminUserListDto[]
        
        isRegistrationActive = false
        newLibrarian: LibrarianRegisterModel

  
        constructor(private service: IUserManagerService, private $scope: ng.IScope) {

            this.subscribeProperties()
        }

        subscribeProperties() {
            var self = this

            self.$scope.$watch(() => self.pageNumber, (newValue: number, oldValue: number) => {

                if (newValue <= 1) 
                    self.pageNumber = 1
                  
                
                if (newValue >= self.totalPagesNumber) 
                    self.pageNumber = self.totalPagesNumber
               

                self.getUserList()
            })
        }
        
        getUserList() {
            
            var self = this

            this.service.getUserList(this.pageNumber).success((usersPageResult: App.IPagedResult<AdminUserListDto[]>) => {
                self.usersPage = usersPageResult.result
                self.totalPagesNumber = usersPageResult.totalPagesNumber
            })
        }

        lockUser(user: AdminUserListDto) {
            
            this.service.lockUser(user.id).success(() => {
                
                user.isLocked = true
                toastr.success("User has been locked successfully")
            })
        }

        unlockUser(user: AdminUserListDto) {

            this.service.unlockUser(user.id).success(() => {

                user.isLocked = false
                toastr.success("User has been unlocked successfully")
            })
        }

        removeUser(user: AdminUserListDto) {
            var self = this

            var confirm = window.confirm("Are you sure you want to remove this user?")

            if (!confirm)
                return

            this.service.removeUser(user.id).success(() => {

                self.usersPage.remove(user)
                toastr.success("User has been removed successfully")
            })
        }

        activeLibrarianRegistration() {
            
            this.newLibrarian = new LibrarianRegisterModel()
            this.isRegistrationActive = true
        }

        registerLibrarian() {
            
            var self = this

            this.service.registerLibrarian(this.newLibrarian).success(() => {
                
                self.isRegistrationActive = false
                self.getUserList()
                toastr.success("New librarian has been registered")
            })
        }
    }

    angular.module("userManager", []).controller("userManagerCtrl", Admin.UserManagerController);
}   