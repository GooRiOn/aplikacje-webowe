/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../../../../scripts/app/dataflow.ts" />
var Admin;
(function (Admin) {
    var GlobalNotificationController = (function () {
        function GlobalNotificationController(service, $scope) {
            this.service = service;
            this.$scope = $scope;
        }
        GlobalNotificationController.prototype.sendGlobalNotification = function () {
            if (!this.message || this.message.length === 0) {
                toastr.warning("Meesage cannot be empty");
                return;
            }
            App.BookInfoHubProxyGetter.instance.sendGlobalNotification(this.message);
            this.message = "";
        };
        GlobalNotificationController.$inject = ["userManagerFactory", "$scope"];
        return GlobalNotificationController;
    })();
    Admin.GlobalNotificationController = GlobalNotificationController;
    angular.module("globalNotification", []).controller("globalNotificationCtrl", Admin.GlobalNotificationController);
})(Admin || (Admin = {}));
//# sourceMappingURL=globalNotificationController.js.map