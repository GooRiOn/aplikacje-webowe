﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../../../../scripts/app/dataflow.ts" />

module Admin {
    export class GlobalNotificationController {

        static $inject = ["userManagerFactory","$scope"]

        message: string

        constructor(private service: IUserManagerService, private $scope: ng.IScope) {
          
        }

        sendGlobalNotification() {
            if (!this.message || this.message.length === 0) {
                toastr.warning("Meesage cannot be empty")
                return
            }

            App.BookInfoHubProxyGetter.instance.sendGlobalNotification(this.message)
            this.message = ""
        }
    }

    angular.module("globalNotification", []).controller("globalNotificationCtrl", Admin.GlobalNotificationController);
}    