/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../../../../scripts/app/dataflow.ts" />
var Admin;
(function (Admin) {
    var UserManagerController = (function () {
        function UserManagerController(service, $scope) {
            this.service = service;
            this.$scope = $scope;
            this.pageNumber = 1;
            this.totalPagesNumber = 1;
            this.isRegistrationActive = false;
            this.subscribeProperties();
        }
        UserManagerController.prototype.subscribeProperties = function () {
            var self = this;
            self.$scope.$watch(function () { return self.pageNumber; }, function (newValue, oldValue) {
                if (newValue <= 1)
                    self.pageNumber = 1;
                if (newValue >= self.totalPagesNumber)
                    self.pageNumber = self.totalPagesNumber;
                self.getUserList();
            });
        };
        UserManagerController.prototype.getUserList = function () {
            var self = this;
            this.service.getUserList(this.pageNumber).success(function (usersPageResult) {
                self.usersPage = usersPageResult.result;
                self.totalPagesNumber = usersPageResult.totalPagesNumber;
            });
        };
        UserManagerController.prototype.lockUser = function (user) {
            this.service.lockUser(user.id).success(function () {
                user.isLocked = true;
                toastr.success("User has been locked successfully");
            });
        };
        UserManagerController.prototype.unlockUser = function (user) {
            this.service.unlockUser(user.id).success(function () {
                user.isLocked = false;
                toastr.success("User has been unlocked successfully");
            });
        };
        UserManagerController.prototype.removeUser = function (user) {
            var self = this;
            var confirm = window.confirm("Are you sure you want to remove this user?");
            if (!confirm)
                return;
            this.service.removeUser(user.id).success(function () {
                self.usersPage.remove(user);
                toastr.success("User has been removed successfully");
            });
        };
        UserManagerController.prototype.activeLibrarianRegistration = function () {
            this.newLibrarian = new Admin.LibrarianRegisterModel();
            this.isRegistrationActive = true;
        };
        UserManagerController.prototype.registerLibrarian = function () {
            var self = this;
            this.service.registerLibrarian(this.newLibrarian).success(function () {
                self.isRegistrationActive = false;
                self.getUserList();
                toastr.success("New librarian has been registered");
            });
        };
        UserManagerController.$inject = ["userManagerFactory", "$scope"];
        return UserManagerController;
    })();
    Admin.UserManagerController = UserManagerController;
    angular.module("userManager", []).controller("userManagerCtrl", Admin.UserManagerController);
})(Admin || (Admin = {}));
//# sourceMappingURL=userManagerController.js.map