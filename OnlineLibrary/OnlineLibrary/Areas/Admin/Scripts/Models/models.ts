﻿module Admin {

    export class AdminUserListDto {

        id: string
        userName: string
        firstName: string
        surname: string
        email: string
        isLocked: boolean
    }

    export class LibrarianRegisterModel {
        username: string
        password: string
        confirmPassword: string

        constructor() {
            var self = this
        }
    }
} 