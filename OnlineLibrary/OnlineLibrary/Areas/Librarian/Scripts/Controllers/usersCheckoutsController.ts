﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../../../../scripts/app/dataflow.ts" />

module Librarian {
    export class UsersCheckoutsController {

        static $inject = ["usersCheckoutsFactory", "$scope"]


        pageNumber = 1
        totalPagesNumber = 1

        checkoutsPage: UserCheckoutDto[]


        checkoutStatusSelection = {

            selectedStatus: { value: "1", name: "Request sent" },
            checkoutStatuses:
            [
                { value: "1", name: "Request sent" },
                { value: "2", name: "Awaiting" },
                { value: "3", name: "Active" },
                { value: "4", name: "Delayed" },
            ]
        }

        selectedCheckoutId: number
        isLoading = false
       
        constructor(private service: IUsersCheckoutsService, private $scope: ng.IScope) {
            this.subscribeProperties()
           
        }

        subscribeProperties() {
            var self = this

            self.$scope.$watch(() => self.pageNumber, (newValue: number, oldValue: number) => {

                if (newValue <= 1)
                    self.pageNumber = 1


                if (newValue >= self.totalPagesNumber)
                    self.pageNumber = self.totalPagesNumber

                self.getCheckoutsPage()
            })

            self.$scope.$watch(() => self.checkoutStatusSelection.selectedStatus, (newValue: any, oldValue: any) => {

                self.getCheckoutsPage()
            })
        }

        getCheckoutsPage() {

            var self = this

            var status = parseInt(self.checkoutStatusSelection.selectedStatus.value)

            self.service.getUsersCheckouts(status, self.pageNumber).success((result: App.IPagedResult<UserCheckoutDto[]>) => {
                
                self.checkoutsPage = result.result
                self.totalPagesNumber = result.totalPagesNumber
            })
        }

        selectCheckout(checkoutId: number) {
            this.selectedCheckoutId = checkoutId
        }

        changeCheckoutStatus(checkout: UserCheckoutDto) {

            var self = this

            self.isLoading = true
            var newCheckoutStatus = checkout.status + 1
           
            self.service.changeCheckoutStatus(checkout.id, newCheckoutStatus).success(() => {
               
                checkout.status = newCheckoutStatus

                var index = self.checkoutsPage.indexOf(checkout)
                self.checkoutsPage.splice(index, 1)

                self.isLoading = false
                toastr.success("Checkout status has been chenged successfully")
            })
        }

        removeCheckout(checkout: UserCheckoutDto) {
            
            var self = this

            var confirm = window.confirm("Are you sure you want to remove this checkout?")

            if (!confirm)
                return

            self.service.removeCheckout(checkout.id).success(() => {
                
                self.checkoutsPage.remove(checkout)
                toastr.success("Checkout has been removed successfully")
            })
        }

        getStatusNameByValue(numericValue: number): string {
            if (!numericValue)
                return

            var value = numericValue.toString()

            return this.checkoutStatusSelection.checkoutStatuses.firstOrNull(c => c.value === value).name
        }

        sendNotification(checkoutId: number) {
            var self = this

            self.isLoading = true

            self.service.sendNotification(checkoutId).success(() => {
                self.isLoading = false
                toastr.success("Notification sent")
            })
        }

    }

    angular.module("usersCheckouts", []).controller("usersCheckoutsCtrl", Librarian.UsersCheckoutsController);
}    