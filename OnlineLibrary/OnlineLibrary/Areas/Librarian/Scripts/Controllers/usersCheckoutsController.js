/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../../../../scripts/app/dataflow.ts" />
var Librarian;
(function (Librarian) {
    var UsersCheckoutsController = (function () {
        function UsersCheckoutsController(service, $scope) {
            this.service = service;
            this.$scope = $scope;
            this.pageNumber = 1;
            this.totalPagesNumber = 1;
            this.checkoutStatusSelection = {
                selectedStatus: { value: "1", name: "Request sent" },
                checkoutStatuses: [
                    { value: "1", name: "Request sent" },
                    { value: "2", name: "Awaiting" },
                    { value: "3", name: "Active" },
                    { value: "4", name: "Delayed" },
                ]
            };
            this.isLoading = false;
            this.subscribeProperties();
        }
        UsersCheckoutsController.prototype.subscribeProperties = function () {
            var self = this;
            self.$scope.$watch(function () { return self.pageNumber; }, function (newValue, oldValue) {
                if (newValue <= 1)
                    self.pageNumber = 1;
                if (newValue >= self.totalPagesNumber)
                    self.pageNumber = self.totalPagesNumber;
                self.getCheckoutsPage();
            });
            self.$scope.$watch(function () { return self.checkoutStatusSelection.selectedStatus; }, function (newValue, oldValue) {
                self.getCheckoutsPage();
            });
        };
        UsersCheckoutsController.prototype.getCheckoutsPage = function () {
            var self = this;
            var status = parseInt(self.checkoutStatusSelection.selectedStatus.value);
            self.service.getUsersCheckouts(status, self.pageNumber).success(function (result) {
                self.checkoutsPage = result.result;
                self.totalPagesNumber = result.totalPagesNumber;
            });
        };
        UsersCheckoutsController.prototype.selectCheckout = function (checkoutId) {
            this.selectedCheckoutId = checkoutId;
        };
        UsersCheckoutsController.prototype.changeCheckoutStatus = function (checkout) {
            var self = this;
            self.isLoading = true;
            var newCheckoutStatus = checkout.status + 1;
            self.service.changeCheckoutStatus(checkout.id, newCheckoutStatus).success(function () {
                checkout.status = newCheckoutStatus;
                var index = self.checkoutsPage.indexOf(checkout);
                self.checkoutsPage.splice(index, 1);
                self.isLoading = false;
                toastr.success("Checkout status has been chenged successfully");
            });
        };
        UsersCheckoutsController.prototype.removeCheckout = function (checkout) {
            var self = this;
            var confirm = window.confirm("Are you sure you want to remove this checkout?");
            if (!confirm)
                return;
            self.service.removeCheckout(checkout.id).success(function () {
                self.checkoutsPage.remove(checkout);
                toastr.success("Checkout has been removed successfully");
            });
        };
        UsersCheckoutsController.prototype.getStatusNameByValue = function (numericValue) {
            if (!numericValue)
                return;
            var value = numericValue.toString();
            return this.checkoutStatusSelection.checkoutStatuses.firstOrNull(function (c) { return c.value === value; }).name;
        };
        UsersCheckoutsController.prototype.sendNotification = function (checkoutId) {
            var self = this;
            self.isLoading = true;
            self.service.sendNotification(checkoutId).success(function () {
                self.isLoading = false;
                toastr.success("Notification sent");
            });
        };
        UsersCheckoutsController.$inject = ["usersCheckoutsFactory", "$scope"];
        return UsersCheckoutsController;
    })();
    Librarian.UsersCheckoutsController = UsersCheckoutsController;
    angular.module("usersCheckouts", []).controller("usersCheckoutsCtrl", Librarian.UsersCheckoutsController);
})(Librarian || (Librarian = {}));
//# sourceMappingURL=usersCheckoutsController.js.map