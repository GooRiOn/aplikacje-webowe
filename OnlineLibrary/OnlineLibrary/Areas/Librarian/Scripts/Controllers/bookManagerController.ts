﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../../../../scripts/app/dataflow.ts" />

module Librarian {
    export class BookManagerController {

        static $inject = ["bookManagerFactory", "$scope"]

      
        pageNumber = 1
        totalPagesNumber = 1
        booksPage: BookDto[]

        currBook: BookDto
        currBookGenre: any
        isBookEditActive = false

        authors: AuthorDto[]
        currAuthor: any

        selectedBookId: number

        isBookAddActive = false


        genreSelection = {
            
            selectedGenre: { value: "5", name: "All" },
            bookGenres:
            [
                { value: "1", name: "Fantasy" },
                { value: "2", name: "Thriller" },
                { value: "3", name: "Drama" },
                { value: "4", name: "Comedy" },
                { value: "5", name: "All" },
            ]
        }

        wantedBookTitle: string
        isSearchByTitleActive = false
        
        constructor(private service: IBookManagerService, private $scope: ng.IScope) {
            this.subscribeProperties()
            this.getAuthors()
        }

        subscribeProperties() {
            var self = this

            self.$scope.$watch(() => self.pageNumber, (newValue: number, oldValue: number) => {

                if (newValue <= 1)
                    self.pageNumber = 1


                if (newValue >= self.totalPagesNumber)
                    self.pageNumber = self.totalPagesNumber


                self.getBookList()
            })

            self.$scope.$watch(() => self.genreSelection.selectedGenre, (newValue: any, oldValue: any) => {
                
                self.getBookList()
            })

            self.$scope.$watch(() => self.wantedBookTitle, (newValue: string, oldValue: string) => {

                if (newValue && newValue.length > 3)
                    self.getBookListByTitle()

                else if (newValue.length === 0) {
                    self.isSearchByTitleActive = false
                    self.getBookList()
                }

            })
        }

        getBookList() {
            var self = this

            var genreNumericValue = parseInt(this.genreSelection.selectedGenre.value)

            this.service.getBookList(genreNumericValue, this.pageNumber).success((bookPageResult: App.IPagedResult<BookDto[]>) => {
                
                self.booksPage = bookPageResult.result
                self.totalPagesNumber = bookPageResult.totalPagesNumber
            })
        }

        getBookListByTitle() {

            var self = this

            self.service.getBookListByTitle(self.wantedBookTitle).success((result: BookDto[]) => {
                self.isSearchByTitleActive = true
                self.booksPage = result
                self.totalPagesNumber = 1
            })
        }

        editBook(book: BookDto) {
            this.currAuthor = this.authors.filter(a => a.id === book.authorId)[0]
            this.currBook = book
            this.currBookGenre = this.genreSelection.bookGenres.filter(b => b.value === book.genre.toString())[0]
            this.isBookEditActive = true
        }

        updateBook() {
            
            this.currBook.genre = parseInt(this.currBookGenre.value)

            if (this.currBook.genre == 5) {
                toastr.warning("Cannot set book genre to 'All'.")
                return
            }

            this.currBook.authorId = this.currAuthor.id

            this.service.updateBook(this.currBook).success(() => {

                this.getBookList()
                this.isBookEditActive = false
                toastr.success("Book has been updated successfully")
            })
        }

        createBook() {
            this.currBook = new BookDto()
            this.isBookAddActive = true;
        }

        addBook() {

            this.currBook.genre = parseInt(this.currBookGenre.value)

            if (this.currBook.genre == 5) {
                toastr.warning("Cannot set book genre to 'All'.")
                return
            }

            this.currBook.authorId = this.currAuthor.id

            if (!this.currBook.graphicId) {
                toastr.warning("Cannot add book without front cover")
                return
            }

            this.service.addBook(this.currBook).success(() => {
                this.getBookList()
                this.isBookAddActive = false
                toastr.success("Book has been added successfully")
            })
        }

        removeBook(book: BookDto) {

            var self = this

            var confirm = window.confirm("Are you sure you want to delete this book?")

            if (!confirm)
                return

            this.service.removeBook(book.id).success(() => {

                self.booksPage.remove(book)
                toastr.success("Book has been successfully removed")
            })
        }

        getAuthors() {
            this.service.getAuthors().success((authors: AuthorDto[]) => {
                this.authors = authors
            })
        }

        selectBook(id: number) {
            this.selectedBookId = id
        }
      
        uploadBookCover(file: any) {
            var formData = new FormData()

            for(var i = 0; i < file.length; i++)
                formData.append("file" + i, file[i])

            this.service.uploadBookCover(formData).success((graphicId: number) => {
                this.currBook.graphicId = graphicId
            })
        }

        getBookGenreNameByValue(numericValue: number) : string {
            
            var value = numericValue.toString()

            return this.genreSelection.bookGenres.firstOrNull(b => b.value === value).name
        }


    }

    angular.module("bookManager", []).controller("bookManagerCtrl", Librarian.BookManagerController);
}   