/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../../../../scripts/app/dataflow.ts" />
var Librarian;
(function (Librarian) {
    var BookManagerController = (function () {
        function BookManagerController(service, $scope) {
            this.service = service;
            this.$scope = $scope;
            this.pageNumber = 1;
            this.totalPagesNumber = 1;
            this.isBookEditActive = false;
            this.isBookAddActive = false;
            this.genreSelection = {
                selectedGenre: { value: "5", name: "All" },
                bookGenres: [
                    { value: "1", name: "Fantasy" },
                    { value: "2", name: "Thriller" },
                    { value: "3", name: "Drama" },
                    { value: "4", name: "Comedy" },
                    { value: "5", name: "All" },
                ]
            };
            this.isSearchByTitleActive = false;
            this.subscribeProperties();
            this.getAuthors();
        }
        BookManagerController.prototype.subscribeProperties = function () {
            var self = this;
            self.$scope.$watch(function () { return self.pageNumber; }, function (newValue, oldValue) {
                if (newValue <= 1)
                    self.pageNumber = 1;
                if (newValue >= self.totalPagesNumber)
                    self.pageNumber = self.totalPagesNumber;
                self.getBookList();
            });
            self.$scope.$watch(function () { return self.genreSelection.selectedGenre; }, function (newValue, oldValue) {
                self.getBookList();
            });
            self.$scope.$watch(function () { return self.wantedBookTitle; }, function (newValue, oldValue) {
                if (newValue && newValue.length > 3)
                    self.getBookListByTitle();
                else if (newValue.length === 0) {
                    self.isSearchByTitleActive = false;
                    self.getBookList();
                }
            });
        };
        BookManagerController.prototype.getBookList = function () {
            var self = this;
            var genreNumericValue = parseInt(this.genreSelection.selectedGenre.value);
            this.service.getBookList(genreNumericValue, this.pageNumber).success(function (bookPageResult) {
                self.booksPage = bookPageResult.result;
                self.totalPagesNumber = bookPageResult.totalPagesNumber;
            });
        };
        BookManagerController.prototype.getBookListByTitle = function () {
            var self = this;
            self.service.getBookListByTitle(self.wantedBookTitle).success(function (result) {
                self.isSearchByTitleActive = true;
                self.booksPage = result;
                self.totalPagesNumber = 1;
            });
        };
        BookManagerController.prototype.editBook = function (book) {
            this.currAuthor = this.authors.filter(function (a) { return a.id === book.authorId; })[0];
            this.currBook = book;
            this.currBookGenre = this.genreSelection.bookGenres.filter(function (b) { return b.value === book.genre.toString(); })[0];
            this.isBookEditActive = true;
        };
        BookManagerController.prototype.updateBook = function () {
            var _this = this;
            this.currBook.genre = parseInt(this.currBookGenre.value);
            if (this.currBook.genre == 5) {
                toastr.warning("Cannot set book genre to 'All'.");
                return;
            }
            this.currBook.authorId = this.currAuthor.id;
            this.service.updateBook(this.currBook).success(function () {
                _this.getBookList();
                _this.isBookEditActive = false;
                toastr.success("Book has been updated successfully");
            });
        };
        BookManagerController.prototype.createBook = function () {
            this.currBook = new Librarian.BookDto();
            this.isBookAddActive = true;
        };
        BookManagerController.prototype.addBook = function () {
            var _this = this;
            this.currBook.genre = parseInt(this.currBookGenre.value);
            if (this.currBook.genre == 5) {
                toastr.warning("Cannot set book genre to 'All'.");
                return;
            }
            this.currBook.authorId = this.currAuthor.id;
            if (!this.currBook.graphicId) {
                toastr.warning("Cannot add book without front cover");
                return;
            }
            this.service.addBook(this.currBook).success(function () {
                _this.getBookList();
                _this.isBookAddActive = false;
                toastr.success("Book has been added successfully");
            });
        };
        BookManagerController.prototype.removeBook = function (book) {
            var self = this;
            var confirm = window.confirm("Are you sure you want to delete this book?");
            if (!confirm)
                return;
            this.service.removeBook(book.id).success(function () {
                self.booksPage.remove(book);
                toastr.success("Book has been successfully removed");
            });
        };
        BookManagerController.prototype.getAuthors = function () {
            var _this = this;
            this.service.getAuthors().success(function (authors) {
                _this.authors = authors;
            });
        };
        BookManagerController.prototype.selectBook = function (id) {
            this.selectedBookId = id;
        };
        BookManagerController.prototype.uploadBookCover = function (file) {
            var _this = this;
            var formData = new FormData();
            for (var i = 0; i < file.length; i++)
                formData.append("file" + i, file[i]);
            this.service.uploadBookCover(formData).success(function (graphicId) {
                _this.currBook.graphicId = graphicId;
            });
        };
        BookManagerController.prototype.getBookGenreNameByValue = function (numericValue) {
            var value = numericValue.toString();
            return this.genreSelection.bookGenres.firstOrNull(function (b) { return b.value === value; }).name;
        };
        BookManagerController.$inject = ["bookManagerFactory", "$scope"];
        return BookManagerController;
    })();
    Librarian.BookManagerController = BookManagerController;
    angular.module("bookManager", []).controller("bookManagerCtrl", Librarian.BookManagerController);
})(Librarian || (Librarian = {}));
//# sourceMappingURL=bookManagerController.js.map