/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../../../../scripts/app/dataflow.ts" />
var Librarian;
(function (Librarian) {
    var UserCheckoutsController = (function () {
        function UserCheckoutsController(service, $scope) {
            this.service = service;
            this.$scope = $scope;
            this.pageNumber = 1;
            this.totalPagesNumber = 1;
            this.checkoutStatusSelection = {
                selectedStatus: { value: "1", name: "Request sent" },
                checkoutStatuses: [
                    { value: "1", name: "Request sent" },
                    { value: "2", name: "Awaiting" },
                    { value: "3", name: "Active" },
                    { value: "4", name: "Delayed" },
                ]
            };
            this.subscribeProperties();
        }
        UserCheckoutsController.prototype.subscribeProperties = function () {
            var self = this;
            self.$scope.$watch(function () { return self.pageNumber; }, function (newValue, oldValue) {
                if (newValue <= 1)
                    self.pageNumber = 1;
                if (newValue >= self.totalPagesNumber)
                    self.pageNumber = self.totalPagesNumber;
                self.getCheckoutsPage();
            });
            self.$scope.$watch(function () { return self.checkoutStatusSelection.selectedStatus; }, function (newValue, oldValue) {
                self.getCheckoutsPage();
            });
            self.$scope.$watch(function () { return self.currCheckoutStatus; }, function (newValue, oldValue) {
                self.changeCheckoutStatus();
            });
        };
        UserCheckoutsController.prototype.getCheckoutsPage = function () {
            var self = this;
            var status = parseInt(self.checkoutStatusSelection.selectedStatus.value);
            self.service.getUsersCheckouts(status, self.pageNumber).success(function (result) {
                self.checkoutsPage = result.result;
                self.totalPagesNumber = result.totalPagesNumber;
            });
        };
        UserCheckoutsController.prototype.selectCheckout = function (checkoutId) {
            this.selectedCheckoutId = checkoutId;
        };
        UserCheckoutsController.prototype.changeCheckoutStatus = function () {
            var self = this;
            self.service.changeCheckoutStatus(self.selectedCheckoutId, self.currCheckoutStatus).success(function () {
                toastr.success("Checkout status has been chenged successfully");
            });
        };
        UserCheckoutsController.prototype.getStatusNameByValue = function (numericValue) {
            if (!numericValue)
                return;
            var value = numericValue.toString();
            return this.checkoutStatusSelection.checkoutStatuses.filter(function (c) { return c.value === value; })[0].name;
        };
        UserCheckoutsController.$inject = ["userCheckoutsFactory", "$scope"];
        return UserCheckoutsController;
    })();
    Librarian.UserCheckoutsController = UserCheckoutsController;
    angular.module("userCheckouts", []).controller("userCheckoutsCtrl", Librarian.UserCheckoutsController);
})(Librarian || (Librarian = {}));
//# sourceMappingURL=userCheckoutsController.js.map