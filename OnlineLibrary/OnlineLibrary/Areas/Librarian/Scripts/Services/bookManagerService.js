/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../scripts/app/dataflow.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Librarian;
(function (Librarian) {
    var BookManagerService = (function (_super) {
        __extends(BookManagerService, _super);
        function BookManagerService($http) {
            _super.call(this, $http);
            this.$http = $http;
        }
        BookManagerService.prototype.getBookList = function (genre, pageNumber) {
            var url = "api/Books/" + genre + "/Genre/" + pageNumber + "/Page";
            return _super.prototype.getJSON.call(this, url, true);
        };
        BookManagerService.prototype.getBookListByTitle = function (title) {
            var url = "api/Books/" + title;
            return _super.prototype.getJSON.call(this, url, false);
        };
        BookManagerService.prototype.addBook = function (book) {
            return _super.prototype.postJSON.call(this, "api/Librarian/Book/Add", book, true);
        };
        BookManagerService.prototype.removeBook = function (id) {
            var url = "api/Librarian/Book/" + id + "/Remove";
            return _super.prototype.postJSON.call(this, url, null, true);
        };
        BookManagerService.prototype.getAuthors = function () {
            return _super.prototype.getJSON.call(this, "api/Authors", true);
        };
        BookManagerService.prototype.updateBook = function (dto) {
            return _super.prototype.postJSON.call(this, "api/Librarian/Book/Update", dto, true);
        };
        BookManagerService.prototype.uploadBookCover = function (formData) {
            return _super.prototype.postFileWithCredentials.call(this, "api/Librarian/Book/Cover/Upload", formData);
        };
        BookManagerService.$inject = ["$http"];
        return BookManagerService;
    })(App.DataService);
    Librarian.BookManagerService = BookManagerService;
    factory.$inject = ["$http"];
    function factory($http) {
        return new BookManagerService($http);
    }
    angular.module("bookManagerService", []).factory("bookManagerFactory", factory);
})(Librarian || (Librarian = {}));
//# sourceMappingURL=bookManagerService.js.map