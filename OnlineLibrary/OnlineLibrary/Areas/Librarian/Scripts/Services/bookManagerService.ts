﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../scripts/app/dataflow.ts" />

module Librarian {

    export interface IBookManagerService {
        getBookList(genre: BookGenreEnum, pageNumber: number): ng.IHttpPromise<App.IPagedResult<BookDto[]>>
        getBookListByTitle(title: string): ng.IHttpPromise<BookDto[]>
        removeBook(id: number): ng.IHttpPromise<any>
        getAuthors(): ng.IHttpPromise<any>
        updateBook(dto: BookDto): ng.IHttpPromise<any>
        addBook(book: BookDto): ng.IHttpPromise<any>
        uploadBookCover(formData: FormData): angular.IHttpPromise<any>
    }

    export class BookManagerService extends App.DataService implements IBookManagerService {

        static $inject = ["$http"]

        constructor(private $http: ng.IHttpService) {
            super($http)
        }

        getBookList(genre : BookGenreEnum,pageNumber: number): ng.IHttpPromise<App.IPagedResult<BookDto[]>> {

            var url = "api/Books/" + genre + "/Genre/" + pageNumber + "/Page"

            return super.getJSON(url, true)
        }

        getBookListByTitle(title: string): ng.IHttpPromise<BookDto[]> {

            var url = "api/Books/" + title

            return super.getJSON(url, false)
        }

        addBook(book: BookDto): ng.IHttpPromise<any> {
            return super.postJSON("api/Librarian/Book/Add", book, true)
        }

        removeBook(id: number): ng.IHttpPromise<any>{

            var url = "api/Librarian/Book/" + id + "/Remove"

            return super.postJSON(url, null, true)
        }

        getAuthors(): ng.IHttpPromise<any> {

            return super.getJSON("api/Authors", true)
        }

        updateBook(dto: BookDto): angular.IHttpPromise<any> {
            
            return super.postJSON("api/Librarian/Book/Update",dto,true)
        }

        uploadBookCover(formData: FormData): angular.IHttpPromise<any> {
            
            return super.postFileWithCredentials("api/Librarian/Book/Cover/Upload",formData)
        }
    }

    factory.$inject = ["$http"]
    function factory($http: ng.IHttpService) {
        return new BookManagerService($http)
    }

    angular.module("bookManagerService", []).factory("bookManagerFactory", factory)
}
  