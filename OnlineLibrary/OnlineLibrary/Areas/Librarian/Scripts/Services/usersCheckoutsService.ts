﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../scripts/app/dataflow.ts" />

module Librarian {

    export interface IUsersCheckoutsService {
        getUsersCheckouts(status: CheckoutStatusEnum, pageNumber: number): ng.IHttpPromise<App.IPagedResult<UserCheckoutDto[]>>
        changeCheckoutStatus(checkoutId: number, status: number): ng.IHttpPromise<any>
        removeCheckout(checkoutId: number): ng.IHttpPromise<any>
        sendNotification(checkoutId: number): ng.IHttpPromise<any> 
    }

    export class UsersCheckoutsService extends App.DataService implements IUsersCheckoutsService {

        static $inject = ["$http"]

        constructor(private $http: ng.IHttpService) {
            super($http)
        }

        getUsersCheckouts(status: CheckoutStatusEnum, pageNumber: number): ng.IHttpPromise<App.IPagedResult<UserCheckoutDto[]>> {
            
            var url = "api/Librarian/Users/Checkouts/" + status + "/Status/" + pageNumber + "/Page"

            return super.getJSON(url,true)
        }

        changeCheckoutStatus(checkoutId: number, status: number): ng.IHttpPromise<any> {
            
            var url = "api/Librarian/Checkout/" + checkoutId + "/" + status + "/Status/Update"

            return super.postJSON(url,null,true)
        }

        removeCheckout(checkoutId: number): ng.IHttpPromise<any> {
            
            var url = "api/Librarian/Checkout/" + checkoutId + "/Remove"

            return super.postJSON(url,null,true)
        }

        sendNotification(checkoutId: number): ng.IHttpPromise<any> {
            var url = "api/Librarian/Checkout/" + checkoutId + "/Notification/Send"

            return super.postJSON(url, null, true)
        }
    }

    factory.$inject = ["$http"]
    function factory($http: ng.IHttpService) {
        return new UsersCheckoutsService($http)
    }

    angular.module("usersCheckoutsService", []).factory("usersCheckoutsFactory", factory)
}
   