/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../scripts/app/dataflow.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Librarian;
(function (Librarian) {
    var UserCheckoutsService = (function (_super) {
        __extends(UserCheckoutsService, _super);
        function UserCheckoutsService($http) {
            _super.call(this, $http);
            this.$http = $http;
        }
        UserCheckoutsService.prototype.getUsersCheckouts = function (status, pageNumber) {
            var url = "api/Librarian/Users/Checkouts/" + status + "/Status/" + pageNumber + "/Page";
            return _super.prototype.getJSON.call(this, url, true);
        };
        UserCheckoutsService.prototype.changeCheckoutStatus = function (checkoutId, status) {
            var url = "api/Librarian/Checkout/" + checkoutId + "/" + status + "/Status/Update";
            return _super.prototype.postJSON.call(this, url, null, true);
        };
        UserCheckoutsService.$inject = ["$http"];
        return UserCheckoutsService;
    })(App.DataService);
    Librarian.UserCheckoutsService = UserCheckoutsService;
    factory.$inject = ["$http"];
    function factory($http) {
        return new UserCheckoutsService($http);
    }
    angular.module("userCheckoutsService", []).factory("userCheckoutsFactory", factory);
})(Librarian || (Librarian = {}));
//# sourceMappingURL=userCheckoutsService.js.map