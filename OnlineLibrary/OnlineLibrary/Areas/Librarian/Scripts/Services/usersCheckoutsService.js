/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../scripts/app/dataflow.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Librarian;
(function (Librarian) {
    var UsersCheckoutsService = (function (_super) {
        __extends(UsersCheckoutsService, _super);
        function UsersCheckoutsService($http) {
            _super.call(this, $http);
            this.$http = $http;
        }
        UsersCheckoutsService.prototype.getUsersCheckouts = function (status, pageNumber) {
            var url = "api/Librarian/Users/Checkouts/" + status + "/Status/" + pageNumber + "/Page";
            return _super.prototype.getJSON.call(this, url, true);
        };
        UsersCheckoutsService.prototype.changeCheckoutStatus = function (checkoutId, status) {
            var url = "api/Librarian/Checkout/" + checkoutId + "/" + status + "/Status/Update";
            return _super.prototype.postJSON.call(this, url, null, true);
        };
        UsersCheckoutsService.prototype.removeCheckout = function (checkoutId) {
            var url = "api/Librarian/Checkout/" + checkoutId + "/Remove";
            return _super.prototype.postJSON.call(this, url, null, true);
        };
        UsersCheckoutsService.prototype.sendNotification = function (checkoutId) {
            var url = "api/Librarian/Checkout/" + checkoutId + "/Notification/Send";
            return _super.prototype.postJSON.call(this, url, null, true);
        };
        UsersCheckoutsService.$inject = ["$http"];
        return UsersCheckoutsService;
    })(App.DataService);
    Librarian.UsersCheckoutsService = UsersCheckoutsService;
    factory.$inject = ["$http"];
    function factory($http) {
        return new UsersCheckoutsService($http);
    }
    angular.module("usersCheckoutsService", []).factory("usersCheckoutsFactory", factory);
})(Librarian || (Librarian = {}));
//# sourceMappingURL=usersCheckoutsService.js.map