var Librarian;
(function (Librarian) {
    (function (BookGenreEnum) {
        BookGenreEnum[BookGenreEnum["Fantasy"] = 1] = "Fantasy";
        BookGenreEnum[BookGenreEnum["Thriller"] = 2] = "Thriller";
        BookGenreEnum[BookGenreEnum["Drama"] = 3] = "Drama";
        BookGenreEnum[BookGenreEnum["Commedy"] = 4] = "Commedy";
        BookGenreEnum[BookGenreEnum["All"] = 5] = "All";
    })(Librarian.BookGenreEnum || (Librarian.BookGenreEnum = {}));
    var BookGenreEnum = Librarian.BookGenreEnum;
    (function (CheckoutStatusEnum) {
        CheckoutStatusEnum[CheckoutStatusEnum["requestSent"] = 1] = "requestSent";
        CheckoutStatusEnum[CheckoutStatusEnum["awaiting"] = 2] = "awaiting";
        CheckoutStatusEnum[CheckoutStatusEnum["active"] = 3] = "active";
        CheckoutStatusEnum[CheckoutStatusEnum["delayed"] = 4] = "delayed";
    })(Librarian.CheckoutStatusEnum || (Librarian.CheckoutStatusEnum = {}));
    var CheckoutStatusEnum = Librarian.CheckoutStatusEnum;
    var BookDto = (function () {
        function BookDto() {
        }
        return BookDto;
    })();
    Librarian.BookDto = BookDto;
    var AuthorDto = (function () {
        function AuthorDto() {
        }
        return AuthorDto;
    })();
    Librarian.AuthorDto = AuthorDto;
    var UserCheckoutDto = (function () {
        function UserCheckoutDto() {
        }
        return UserCheckoutDto;
    })();
    Librarian.UserCheckoutDto = UserCheckoutDto;
})(Librarian || (Librarian = {}));
//# sourceMappingURL=models.js.map