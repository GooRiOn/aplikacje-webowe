﻿using System.Web.Mvc;
using OnlineLibrary.Auth;

namespace OnlineLibrary.Areas.Librarian.Controllers
{
    [OnlyLibrarianMvc]
    public class RootController : Controller
    {
        // GET: Librarian/Root
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BookManager()
        {
            return View();
        }

        public ActionResult UsersCheckouts()
        {
            return View();
        }
    }
}