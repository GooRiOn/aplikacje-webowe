﻿using System.Web.Mvc;

namespace OnlineLibrary.Areas.Home.Controllers
{
    public class RootController : Controller
    {
        // GET: Home/Root
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Home()
        {
            return View();
        }
    }
}