﻿module Home {

    export class RegisterDto {
        username: string
        email: string
        password: string
        confirmPassword: string
    }

    export class LoginDto {
        username: string
        password: string
        grant_type: string
    }

}