﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 

module Home {

    export interface IAuthService {
        register(registerDto: RegisterDto): ng.IHttpPromise<any>
        login(loginDto: LoginDto): ng.IHttpPromise<any>
        getUserInfo(): ng.IHttpPromise<any>
        logout(): ng.IHttpPromise<any>
    }

    export class AuthService extends App.DataService implements IAuthService{

        static $inject = ["$http"]

        constructor(private $http: ng.IHttpService) {
            super($http)
        }

        register(registerDto: RegisterDto): ng.IHttpPromise<any> {

            return super.postJSON("api/Account/Register",registerDto,false)
        }

        login(loginDto: LoginDto): ng.IHttpPromise<any> {
            return super.postJSONwithQueryParams("Token",loginDto,false)
        }

        logout(): ng.IHttpPromise<any> {
            
            return super.postJSON("api/Account/Logout",null,true)
        }

        getUserInfo(): ng.IHttpPromise<any> {
            return super.getJSON("api/Account/UserInfo",true)
        }
    }

    factory.$inject = ["$http"]
    function factory($http: ng.IHttpService)
    {
        return new AuthService($http)
    }

    angular.module("homeAuthService",[]).factory("authServiceFactory",factory)
}
 