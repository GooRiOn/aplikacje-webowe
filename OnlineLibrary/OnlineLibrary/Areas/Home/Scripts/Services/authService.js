/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Home;
(function (Home) {
    var AuthService = (function (_super) {
        __extends(AuthService, _super);
        function AuthService($http) {
            _super.call(this, $http);
            this.$http = $http;
        }
        AuthService.prototype.register = function (registerDto) {
            return _super.prototype.postJSON.call(this, "api/Account/Register", registerDto, false);
        };
        AuthService.prototype.login = function (loginDto) {
            return _super.prototype.postJSONwithQueryParams.call(this, "Token", loginDto, false);
        };
        AuthService.prototype.logout = function () {
            return _super.prototype.postJSON.call(this, "api/Account/Logout", null, true);
        };
        AuthService.prototype.getUserInfo = function () {
            return _super.prototype.getJSON.call(this, "api/Account/UserInfo", true);
        };
        AuthService.$inject = ["$http"];
        return AuthService;
    })(App.DataService);
    Home.AuthService = AuthService;
    factory.$inject = ["$http"];
    function factory($http) {
        return new AuthService($http);
    }
    angular.module("homeAuthService", []).factory("authServiceFactory", factory);
})(Home || (Home = {}));
//# sourceMappingURL=authService.js.map