var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Home;
(function (Home) {
    var AuthService = (function (_super) {
        __extends(AuthService, _super);
        function AuthService($http) {
            _super.call(this, $http);
            this.$http = $http;
        }
        AuthService.prototype.register = function () {
            return _super.prototype.getJSON.call(this, "api/Test/Authors", false);
        };
        AuthService.$inject = ["$http"];
        return AuthService;
    })(App.DataService);
    Home.AuthService = AuthService;
    factory.$inject = ["$http"];
    function factory($http) {
        return new AuthService($http);
    }
    angular.module("app").factory("authServiceFactory", factory);
})(Home || (Home = {}));
//# sourceMappingURL=testService.js.map