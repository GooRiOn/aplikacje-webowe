﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Services/authService.ts" />
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />

module Home {
    export class RegisterController {
        
        static $inject = ["authServiceFactory", "$scope"];
        authService: IAuthService

        username: string
        email: string
        password: string
        confirmPassword: string

        constructor(authService: IAuthService, $scope: ng.IScope) {
            this.authService = authService
        }

        register() {
            
            var registerDto: RegisterDto = {

                username: this.username,
                email: this.email,
                password: this.password,
                confirmPassword: this.confirmPassword
            }
            
            this.authService.register(registerDto)
                .success(() => { toastr.success('User has been registered successfully') })
                .error(() => { toastr.error("Invalid data") })
            
        }
    }

    angular.module("register",[]).controller("registerCtrl", RegisterController);
} 