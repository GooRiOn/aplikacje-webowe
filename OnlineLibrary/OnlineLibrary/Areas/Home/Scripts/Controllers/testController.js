/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Services/testService.ts" />
var TestModule;
(function (TestModule) {
    var TSDemoController = (function () {
        function TSDemoController(testService, $scope) {
            this.testService = testService;
            this.testVar = 'DZIAŁA!!!';
        }
        TSDemoController.prototype.Test = function () {
            var _this = this;
            this.result = this.testService.test().success(function (result) {
                _this.result = result;
            });
        };
        TSDemoController.$inject = ["testFactory", "$scope"];
        return TSDemoController;
    })();
    TestModule.TSDemoController = TSDemoController;
    angular.module("home", []).controller("testCtrl", TSDemoController);
})(TestModule || (TestModule = {}));
//# sourceMappingURL=testController.js.map