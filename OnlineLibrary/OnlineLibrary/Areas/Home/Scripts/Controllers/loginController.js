/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Services/authService.ts" />
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
var Home;
(function (Home) {
    var LoginController = (function () {
        function LoginController(authService, $scope) {
            this.authService = authService;
            this.auth = App.Auth.getInstance();
        }
        LoginController.prototype.login = function () {
            var _this = this;
            var self = this;
            var loginDto = {
                username: self.username,
                password: self.password,
                grant_type: "password"
            };
            this.authService.login(loginDto)
                .success(function (result) {
                self.auth.setAccessToken(result.access_token, false);
                _this.getUserInfo();
                history.pushState(null, null, "/");
            })
                .error(function () { toastr.error("Invalid data"); });
        };
        LoginController.prototype.getUserInfo = function () {
            var _this = this;
            this.authService.getUserInfo().success(function (result) {
                _this.auth.user = result;
            });
        };
        LoginController.$inject = ["authServiceFactory", "$scope"];
        return LoginController;
    })();
    Home.LoginController = LoginController;
    angular.module("login", []).controller("loginCtrl", LoginController);
})(Home || (Home = {}));
//# sourceMappingURL=loginController.js.map