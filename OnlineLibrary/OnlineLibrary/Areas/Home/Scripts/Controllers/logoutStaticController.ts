﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Services/authService.ts" />
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />

module Home {
    export class LogoutStaticController {

        static $inject = ["authServiceFactory"];
        authService: IAuthService
        auth: App.IAuth

        constructor(authService: IAuthService) {
            this.authService = authService
            this.auth = App.Auth.getInstance()
        }

        logout() {
            
            this.authService.logout().success(() => {
                this.auth.clearAccessToken()
                this.auth.user = null
            })
        }
    }

    angular.module("logout", []).controller("logoutStaticCtrl", LogoutStaticController);
}   