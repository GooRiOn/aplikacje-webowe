/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Services/authService.ts" />
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
var Home;
(function (Home) {
    var RegisterController = (function () {
        function RegisterController(authService, $scope) {
            this.authService = authService;
        }
        RegisterController.prototype.register = function () {
            var registerDto = {
                username: this.username,
                email: this.email,
                password: this.password,
                confirmPassword: this.confirmPassword
            };
            this.authService.register(registerDto)
                .success(function () { toastr.success('User has been registered successfully'); })
                .error(function () { toastr.error("Invalid data"); });
        };
        RegisterController.$inject = ["authServiceFactory", "$scope"];
        return RegisterController;
    })();
    Home.RegisterController = RegisterController;
    angular.module("register", []).controller("registerCtrl", RegisterController);
})(Home || (Home = {}));
//# sourceMappingURL=registerController.js.map