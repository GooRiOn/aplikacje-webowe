﻿/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Services/authService.ts" />
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />

module Home {
    export class LoginController {

        static $inject = ["authServiceFactory", "$scope"];
        authService: IAuthService

        username: string
        password: string
        auth: App.IAuth

        constructor(authService: IAuthService, $scope: ng.IScope) {
            this.authService = authService
            this.auth = App.Auth.getInstance()
        }

        login() {
            var self = this

            var loginDto: LoginDto = {
                username: self.username,
                password: self.password,
                grant_type:"password"
            }

            this.authService.login(loginDto)
                .success((result: any) => {
                    
                    self.auth.setAccessToken(result.access_token,false);
                    this.getUserInfo()
                    history.pushState(null,null,"/")
                })
                .error(() => { toastr.error("Invalid data") })
        }

        getUserInfo() {
            this.authService.getUserInfo().success((result: App.IUser) => {
                this.auth.user = result
            })
        }
    }

    angular.module("login", []).controller("loginCtrl", LoginController);
}  