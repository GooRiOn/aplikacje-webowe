/// <reference path="../../../../Scripts/typings/angularjs/angular.d.ts" /> 
/// <reference path="../../../../Scripts/app/auth.ts" /> 
/// <reference path="../Services/authService.ts" />
/// <reference path="../Models/models.ts"/>
/// <reference path="../../../../scripts/typings/toastr/toastr.d.ts" />
var Home;
(function (Home) {
    var LogoutStaticController = (function () {
        function LogoutStaticController(authService) {
            this.authService = authService;
            this.auth = App.Auth.getInstance();
        }
        LogoutStaticController.prototype.logout = function () {
            var _this = this;
            this.authService.logout().success(function () {
                _this.auth.clearAccessToken();
                _this.auth.user = null;
            });
        };
        LogoutStaticController.$inject = ["authServiceFactory"];
        return LogoutStaticController;
    })();
    Home.LogoutStaticController = LogoutStaticController;
    angular.module("logout", []).controller("logoutStaticCtrl", LogoutStaticController);
})(Home || (Home = {}));
//# sourceMappingURL=logoutStaticController.js.map