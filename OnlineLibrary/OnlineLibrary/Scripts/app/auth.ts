﻿module App {

    export interface IAuth {
        user: IUser
        isSessionStored: boolean
        accessToken: string
        setAccessToken(accessToken: string, isSessionStored: boolean): void
        getAccessToken(): string
        clearAccessToken(): void
        isUserAdmin(): boolean
        isUserLibrarian(): boolean
    }

    export interface IUser {
        username: string
        userRoles: any[]
        isLoggedIn : boolean
    }
    
    export class Auth implements IAuth{

        user : IUser

        constructor() {
            this.accessToken = ""
        }

        setAccessToken(accessToken: string, isSessionStored: boolean): void {

            if (isSessionStored) {
                sessionStorage.setItem('accessToken', accessToken)
                localStorage.clear()
            }
            else {
                localStorage.setItem('accessToken', accessToken)
                sessionStorage.clear()
            }

            this.accessToken = accessToken
            this.isSessionStored = isSessionStored
        }

        getAccessToken(): string {
            if (this.isSessionStored)
                return sessionStorage.getItem('accessToken')
            else
                return localStorage.getItem('accessToken')
        }

        clearAccessToken(): void {
            localStorage.clear()
            sessionStorage.clear()
            document.cookie = ""
            this.setAccessToken("", true)
        }

        isUserAdmin(): boolean {

            if(!this.user || !this.user.isLoggedIn) return false
            return this.user.userRoles.some(r => r === 'admin')
        }

        isUserLibrarian(): boolean {

            if (!this.user || !this.user.isLoggedIn) return false
            return this.user.userRoles.some(r => r === 'librarian')
        }

        static getInstance() : Auth{

            if (!this._instance) 
                this._instance = new Auth()
            
            return this._instance
        }

        static _instance: Auth

        accessToken: string;
        isSessionStored: boolean;
    }
    
    export class AuthScopeGetter {

        scope: IAuth

        static $inject = ["$scope"]

        constructor($scope: ng.IScope) {
            this.scope = Auth.getInstance()
        }
    }
    
    angular.module("authScopeGetter", []).controller("authScopeGetter", AuthScopeGetter);
} 