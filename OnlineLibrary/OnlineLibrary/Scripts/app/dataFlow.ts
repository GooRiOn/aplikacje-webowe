﻿module App {

    export interface IPagedResult<TResult> {

        result: TResult
        totalPagesNumber: number
    }
} 