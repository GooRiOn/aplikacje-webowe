﻿/// <reference path="../typings/angularjs/angular.d.ts" />
((): void=> {
    var app = angular.module("app", ['ui.router', 'rootModules', 'authScopeGetter']);
    app.config(App.Routes.configureRoutes);
    App.BookInfoHubProxyGetter.instance = new App.GlobalNotificationHubProxy()
    App.BookInfoHubProxyGetter.instance.connectToGlobalNotificationHub()
})() 
