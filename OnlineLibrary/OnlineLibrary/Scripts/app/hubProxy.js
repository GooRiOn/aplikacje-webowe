/// <reference path="../typings/signalr/signalr.d.ts" />
var App;
(function (App) {
    var GlobalNotificationHubProxy = (function () {
        function GlobalNotificationHubProxy() {
            this.globalNotificationConnection = $.hubConnection();
            this.globalNotificationHubProxy = this.globalNotificationConnection.createHubProxy('GlobalNotificationHub');
        }
        GlobalNotificationHubProxy.prototype.sendGlobalNotification = function (message) {
            this.globalNotificationHubProxy.invoke('SendGlobalNotification', message);
        };
        GlobalNotificationHubProxy.prototype.connectToGlobalNotificationHub = function () {
            var self = this;
            self.globalNotificationHubProxy.on('sendGlobalNotification', function (message) {
                toastr.success(message);
            });
            self.globalNotificationConnection.start();
        };
        return GlobalNotificationHubProxy;
    })();
    App.GlobalNotificationHubProxy = GlobalNotificationHubProxy;
    var BookInfoHubProxyGetter = (function () {
        function BookInfoHubProxyGetter() {
        }
        return BookInfoHubProxyGetter;
    })();
    App.BookInfoHubProxyGetter = BookInfoHubProxyGetter;
})(App || (App = {}));
//# sourceMappingURL=hubProxy.js.map