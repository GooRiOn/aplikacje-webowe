﻿
module App {

    export interface IDataService {
        getJSON<T>(url: string, isAccessTokenRequired: boolean): ng.IHttpPromise<T>
        postJSON<T>(url: string, data: any, isAccessTokenRequired: boolean): ng.IHttpPromise<T>
        postJSONwithQueryParams<T>(url: string, data: any, isAccessTokenRequired: boolean): ng.IHttpPromise<T>
        postFileWithCredentials<T>(url: string, formData: FormData): ng.IHttpPromise<T>
    }

    export class DataService implements IDataService{

        http: ng.IHttpService
        auth: Auth
        
        constructor($http : ng.IHttpService) {
            this.http = $http
            this.auth = Auth.getInstance()
        }

        getJSON<T>(url: string, isAccessTokenRequired: boolean): ng.IHttpPromise<T> {
            
            var authenticationParams: ng.IRequestShortcutConfig = {}

            if (isAccessTokenRequired) {
                var accessToken = this.auth.getAccessToken()
                authenticationParams.headers = { 'Authorization': 'Bearer ' + accessToken }   
            }

            return this.http.get<T>(url,authenticationParams)
        }

        postJSON<T>(url: string, data: any, isAccessTokenRequired: boolean): ng.IHttpPromise<T>{

            var authenticationParams: ng.IRequestShortcutConfig = {}

            if (isAccessTokenRequired) {
                
                var accessToken = this.auth.getAccessToken()
                authenticationParams.headers = { 'Authorization': 'Bearer ' + accessToken }
            }

            return this.http.post<T>(url,data,authenticationParams)
        }

        postJSONwithQueryParams<T>(url: string, data: any, isAccessTokenRequired: boolean): ng.IHttpPromise<T> {

            var authenticationParams: ng.IRequestShortcutConfig = {}

            if (isAccessTokenRequired) {

                var accessToken = this.auth.getAccessToken()
                authenticationParams.headers = { 'Authorization': 'Bearer ' + accessToken }
            }

            return this.http.post<T>(url, $.param(data), authenticationParams)
        }

        postFileWithCredentials<T>(url: string, formData: FormData): ng.IHttpPromise<T> {

            var accessToken = this.auth.getAccessToken()

            if (!accessToken)
                return

            return this.http.post<T>(url, formData, {
                withCredentials: true,
                headers: {
                    'Content-Type': undefined,
                    'Authorization': 'Bearer ' + accessToken
                },
                transformRequest: angular.identity
            })
        }
        
    }
}