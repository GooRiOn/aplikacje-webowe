var App;
(function (App) {
    var DataService = (function () {
        function DataService($http) {
            this.http = $http;
            this.auth = App.Auth.getInstance();
        }
        DataService.prototype.getJSON = function (url, isAccessTokenRequired) {
            var authenticationParams = {};
            if (isAccessTokenRequired) {
                var accessToken = this.auth.getAccessToken();
                authenticationParams.headers = { 'Authorization': 'Bearer ' + accessToken };
            }
            return this.http.get(url, authenticationParams);
        };
        DataService.prototype.postJSON = function (url, data, isAccessTokenRequired) {
            var authenticationParams = {};
            if (isAccessTokenRequired) {
                var accessToken = this.auth.getAccessToken();
                authenticationParams.headers = { 'Authorization': 'Bearer ' + accessToken };
            }
            return this.http.post(url, data, authenticationParams);
        };
        DataService.prototype.postJSONwithQueryParams = function (url, data, isAccessTokenRequired) {
            var authenticationParams = {};
            if (isAccessTokenRequired) {
                var accessToken = this.auth.getAccessToken();
                authenticationParams.headers = { 'Authorization': 'Bearer ' + accessToken };
            }
            return this.http.post(url, $.param(data), authenticationParams);
        };
        DataService.prototype.postFileWithCredentials = function (url, formData) {
            var accessToken = this.auth.getAccessToken();
            if (!accessToken)
                return;
            return this.http.post(url, formData, {
                withCredentials: true,
                headers: {
                    'Content-Type': undefined,
                    'Authorization': 'Bearer ' + accessToken
                },
                transformRequest: angular.identity
            });
        };
        return DataService;
    })();
    App.DataService = DataService;
})(App || (App = {}));
//# sourceMappingURL=dataService.js.map