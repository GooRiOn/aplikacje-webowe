/// <reference path="../typings/angularjs/angular.d.ts" />
/// <reference path="../typings/angular-ui-router/angular-ui-router.d.ts" />
var App;
(function (App) {
    var Routes = (function () {
        function Routes() {
        }
        Routes.configureRoutes = function ($urlRouterProvider, $stateProvider) {
            $urlRouterProvider.otherwise('/');
            $stateProvider.state('home', { url: '/', templateUrl: '/Home/Root/Home' });
            $stateProvider.state('register', { url: '/register', templateUrl: '/Home/Root/Register', controller: 'registerCtrl', controllerAs: "register" });
            $stateProvider.state('login', { url: '/login', templateUrl: '/Home/Root/Login', controller: 'loginCtrl', controllerAs: "login" });
            $stateProvider.state('userManager', { url: '/userManager', templateUrl: '/Admin/Root/UserManager', controller: 'userManagerCtrl', controllerAs: "userManager" });
            $stateProvider.state('globalNotification', { url: '/globalNotification', templateUrl: '/Admin/Root/GlobalNotification', controller: 'globalNotificationCtrl', controllerAs: "globalNotification" });
            $stateProvider.state('bookManager', { url: '/bookManager', templateUrl: '/Librarian/Root/BookManager', controller: 'bookManagerCtrl', controllerAs: "bookManager" });
            $stateProvider.state('bookCatalog', { url: '/bookCatalog', templateUrl: '/User/Root/BookCatalog', controller: 'bookCatalogCtrl', controllerAs: "bookCatalog" });
            $stateProvider.state('bookInfo', { url: '/book/{id}', templateUrl: '/User/Root/BookInfo', controller: 'bookInfoCtrl', controllerAs: "bookInfo" });
            $stateProvider.state('myCheckouts', { url: '/me/checkouts', templateUrl: '/User/Root/MyCheckouts', controller: 'myCheckoutsCtrl', controllerAs: "myCheckouts" });
            $stateProvider.state('usersCheckouts', { url: '/usersCheckouts', templateUrl: '/Librarian/Root/UsersCheckouts', controller: 'usersCheckoutsCtrl', controllerAs: "usersCheckouts" });
        };
        Routes.$inject = ["$urlRouterProvider,$stateProvider"];
        return Routes;
    })();
    App.Routes = Routes;
})(App || (App = {}));
//# sourceMappingURL=app.Routes.js.map