﻿module App {

    export class Lazy<T> {

        private _isValueCreated = false
        private _value: T = null

        constructor(private valueFactory: () => T) {

        }

        get isValueCreated(): boolean {
            return this._isValueCreated
        }

        get value(): T {

            if (this._isValueCreated)
                return this._value

            this._value = this.valueFactory()

            this._isValueCreated = true

            return this._value
        }

    }
} 