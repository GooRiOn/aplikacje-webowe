var App;
(function (App) {
    var Auth = (function () {
        function Auth() {
            this.accessToken = "";
        }
        Auth.prototype.setAccessToken = function (accessToken, isSessionStored) {
            if (isSessionStored) {
                sessionStorage.setItem('accessToken', accessToken);
                localStorage.clear();
            }
            else {
                localStorage.setItem('accessToken', accessToken);
                sessionStorage.clear();
            }
            this.accessToken = accessToken;
            this.isSessionStored = isSessionStored;
        };
        Auth.prototype.getAccessToken = function () {
            if (this.isSessionStored)
                return sessionStorage.getItem('accessToken');
            else
                return localStorage.getItem('accessToken');
        };
        Auth.prototype.clearAccessToken = function () {
            localStorage.clear();
            sessionStorage.clear();
            document.cookie = "";
            this.setAccessToken("", true);
        };
        Auth.prototype.isUserAdmin = function () {
            if (!this.user || !this.user.isLoggedIn)
                return false;
            return this.user.userRoles.some(function (r) { return r === 'admin'; });
        };
        Auth.prototype.isUserLibrarian = function () {
            if (!this.user || !this.user.isLoggedIn)
                return false;
            return this.user.userRoles.some(function (r) { return r === 'librarian'; });
        };
        Auth.getInstance = function () {
            if (!this._instance)
                this._instance = new Auth();
            return this._instance;
        };
        return Auth;
    })();
    App.Auth = Auth;
    var AuthScopeGetter = (function () {
        function AuthScopeGetter($scope) {
            this.scope = Auth.getInstance();
        }
        AuthScopeGetter.$inject = ["$scope"];
        return AuthScopeGetter;
    })();
    App.AuthScopeGetter = AuthScopeGetter;
    angular.module("authScopeGetter", []).controller("authScopeGetter", AuthScopeGetter);
})(App || (App = {}));
//# sourceMappingURL=auth.js.map