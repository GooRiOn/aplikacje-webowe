var App;
(function (App) {
    var Lazy = (function () {
        function Lazy(valueFactory) {
            this.valueFactory = valueFactory;
            this._isValueCreated = false;
            this._value = null;
        }
        Object.defineProperty(Lazy.prototype, "isValueCreated", {
            get: function () {
                return this._isValueCreated;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Lazy.prototype, "value", {
            get: function () {
                if (this._isValueCreated)
                    return this._value;
                this._value = this.valueFactory();
                this._isValueCreated = true;
                return this._value;
            },
            enumerable: true,
            configurable: true
        });
        return Lazy;
    })();
    App.Lazy = Lazy;
})(App || (App = {}));
//# sourceMappingURL=lazy.js.map