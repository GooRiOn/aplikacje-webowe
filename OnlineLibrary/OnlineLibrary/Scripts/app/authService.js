var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var App;
(function (App) {
    var AuthService = (function (_super) {
        __extends(AuthService, _super);
        function AuthService($http) {
            _super.call(this, $http);
            this.$http = $http;
        }
        AuthService.prototype.getUserInfo = function () {
            return _super.prototype.getJSON.call(this, "api/Account/UserInfo", true);
        };
        AuthService.$inject = ["$http"];
        return AuthService;
    })(App.DataService);
    App.AuthService = AuthService;
})(App || (App = {}));
//# sourceMappingURL=authService.js.map