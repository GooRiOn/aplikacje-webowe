/// <reference path="../typings/infusejs.d.ts" />
/// <reference path="auth.ts"/>>
var App;
(function (App) {
    var DependencyInjectionRegistration = (function () {
        function DependencyInjectionRegistration() {
            this.register();
        }
        DependencyInjectionRegistration.prototype.register = function () {
            this.injector = new infuse.Injector();
            this.injector.mapClass('Auth', App.lazyAuth, true);
        };
        return DependencyInjectionRegistration;
    })();
    App.DependencyInjectionRegistration = DependencyInjectionRegistration;
})(App || (App = {}));
//# sourceMappingURL=dependencyInjectionRegistration.js.map