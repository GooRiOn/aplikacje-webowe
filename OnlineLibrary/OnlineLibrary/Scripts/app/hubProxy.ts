﻿/// <reference path="../typings/signalr/signalr.d.ts" />

module App {
    
    export interface IGlobalNotificationHubProxy {
        
        globalNotificationConnection: HubConnection
        globalNotificationHubProxy: HubProxy
        connectToGlobalNotificationHub(): void
    }

    export class GlobalNotificationHubProxy implements IGlobalNotificationHubProxy {
        
        globalNotificationConnection: HubConnection
        globalNotificationHubProxy: HubProxy

        constructor() {
            this.globalNotificationConnection = $.hubConnection()
            this.globalNotificationHubProxy = this.globalNotificationConnection.createHubProxy('GlobalNotificationHub')
        }

        sendGlobalNotification(message: string) {
            this.globalNotificationHubProxy.invoke('SendGlobalNotification', message)
        }

        connectToGlobalNotificationHub() {

            var self = this

            self.globalNotificationHubProxy.on('sendGlobalNotification', (message: string) => {
                toastr.success(message)
            })

            self.globalNotificationConnection.start()
        }

        
    }

    export class BookInfoHubProxyGetter {
        
        static instance: GlobalNotificationHubProxy
    }
} 