﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace OnlineLibrary.Results
{
    public class BinaryDataResult : IHttpActionResult
    {
        public readonly byte[] Data;
        public readonly string ContentType;
        public string ContentDisposition;
        public string FileName;
        public DateTime? CreatedDate;

        public BinaryDataResult(byte[] data, string contentType)
        {
            Data = data;
            ContentType = contentType;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ByteArrayContent(Data)
                };

                if (!String.IsNullOrWhiteSpace(ContentDisposition))
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue(ContentDisposition)
                    {
                        CreationDate = CreatedDate,
                        FileName = FileName
                    };

                response.Content.Headers.ContentType = new MediaTypeHeaderValue(ContentType);

                return response;

            }, cancellationToken);
        }
    }
}