﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using OnlineLibrary.Domain.Services.Interfaces;

namespace OnlineLibrary
{
    public class Init 
    {
        private readonly IAuthorService _authorService;

        public Init(IAuthorService authorService)
        {
            _authorService = authorService;
        }
        
        public void InitDb()
        {
            _authorService.GetAuthorsAsync();
        }
    }
}
