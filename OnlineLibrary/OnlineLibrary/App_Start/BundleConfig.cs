﻿using System.Web;
using System.Web.Optimization;

namespace OnlineLibrary
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/toastr").Include(
                        "~/Scripts/toastr.js"));
            
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/toastr.css"));

            bundles.Add(new ScriptBundle("~/bundles/libraries").Include(
                      "~/Scripts/angular.js",
                      "~/Scripts/angular-ui-router.js",
                      "~/Scripts/angular-route.js",
                      "~/Scripts/jquery.signalR-2.2.0.js",
                      "~/Scripts/infuse.js",
                      "~/Scripts/app/app.Routes.js",
                       "~/Scripts/app/auth.js",
                       "~/Scripts/app/hubProxy.js",
                      "~/Scripts/app/app.js",
                      "~/Scripts/app/rootModules.js",
                      "~/Scripts/app/dataService.js",
                      "~/Scripts/app/dataFlow.js",
                      "~/Scripts/app/collectionExtensions.js"
                      
                ));

            bundles.Add(new ScriptBundle("~/bundles/home").Include(
                      "~/Areas/Home/Scripts/Models/models.js",
                      "~/Areas/Home/Scripts/rootModule.js",
                      "~/Areas/Home/Scripts/Services/authService.js",
                      "~/Areas/Home/Scripts/Controllers/registerController.js",
                      "~/Areas/Home/Scripts/Controllers/loginController.js",
                      "~/Areas/Home/Scripts/Controllers/logoutStaticController.js"
                      
                ));


            bundles.Add(new ScriptBundle("~/bundles/admin").Include(
                      "~/Areas/Admin/Scripts/Models/models.js",
                      "~/Areas/Admin/Scripts/rootModule.js",
                      "~/Areas/Admin/Scripts/Services/userManagerService.js",
                      "~/Areas/Admin/Scripts/Controllers/userManagerController.js",
                      "~/Areas/Admin/Scripts/Controllers/globalNotificationController.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/librarian").Include(
                      "~/Areas/Librarian/Scripts/Models/models.js",
                      "~/Areas/Librarian/Scripts/rootModule.js",
                      "~/Areas/Librarian/Scripts/Services/bookManagerService.js",
                      "~/Areas/Librarian/Scripts/Services/usersCheckoutsService.js",
                      "~/Areas/Librarian/Scripts/Controllers/bookManagerController.js",
                      "~/Areas/Librarian/Scripts/Controllers/usersCheckoutsController.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/user").Include(
                     "~/Areas/User/Scripts/Models/models.js",
                     "~/Areas/User/Scripts/rootModule.js",
                     "~/Areas/User/Scripts/Services/bookCatalogService.js",
                     "~/Areas/User/Scripts/Services/bookInfoService.js",
                      "~/Areas/User/Scripts/Services/myCheckoutsService.js",
                     "~/Areas/User/Scripts/Controllers/bookCatalogController.js",
                     "~/Areas/User/Scripts/Controllers/bookInfoController.js",
                     "~/Areas/User/Scripts/Controllers/myCheckoutsController.js"
               ));
        }
    }
}
