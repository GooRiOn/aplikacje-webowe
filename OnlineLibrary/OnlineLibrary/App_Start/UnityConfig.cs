using System;
using System.Collections.Generic;
using Microsoft.Practices.Unity;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using OnlineLibrary.Domain.DependencyInjection;
using OnlineLibrary.Domain.Services.Interfaces;
using OnlineLibrary.Providers;
using Unity.WebApi;

namespace OnlineLibrary
{
    public interface IDependencyResolver
    {
        TResolved Resolve<TResolved>();
        TResolved Resolve<TResolved, TOverride>(TOverride @override);
        TResolved Resolve<TResolved, TOverride0, TOverride1>(TOverride0 @override0, TOverride1 @override1);
        TResolved Resolve<TResolved, TOverride0, TOverride1, TOverride2>(TOverride0 @override0, TOverride1 @override1, TOverride2 @override2);
        object Resolve(Type serviceType);
        IEnumerable<object> ResolveAll(Type serviceType);
        void BuildUp<TBuildUpTarget>(TBuildUpTarget buildUpTarget);
        void BuildUp(object buildUpTarget);
    }

    class CustomDependencyResolver : IDependencyResolver
    {
        private readonly UnityContainer _container;
        public CustomDependencyResolver(UnityContainer container)
        {
            _container = container;
        }

        public void BuildUp<TTarget>(TTarget target)
        {
            _container.BuildUp<TTarget>(target);
        }

        public void BuildUp(object target)
        {
            _container.BuildUp(target);
        }

        public TResolved Resolve<TResolved>()
        {
            return _container.Resolve<TResolved>();
        }

        public object Resolve(Type serviceType)
        {
            return _container.Resolve(serviceType);
        }

        public IEnumerable<object> ResolveAll(Type serviceType)
        {
            return _container.ResolveAll(serviceType);
        }

        public TResolved Resolve<TResolved, TOverride>(TOverride @override)
        {
            return _container.Resolve<TResolved>(new DependencyOverride<TOverride>(@override));
        }

        public TResolved Resolve<TResolved, TOverride0, TOverride1>(TOverride0 @override0, TOverride1 @override1)
        {
            return _container.Resolve<TResolved>(new DependencyOverride<TOverride0>(@override0), new DependencyOverride<TOverride1>(@override1));
        }

        public TResolved Resolve<TResolved, TOverride0, TOverride1, TOverride2>(TOverride0 @override0, TOverride1 @override1, TOverride2 @override2)
        {
            return _container.Resolve<TResolved>(new DependencyOverride<TOverride0>(@override0), new DependencyOverride<TOverride1>(@override1), new DependencyOverride<TOverride2>(@override2));
        }
    }

    public partial class Startup
    {
        private IDependencyResolver RegisterComponents()
        {
			var container = new UnityContainer();

            DependencyInjectionRegistration.RegisterType(container);
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);

            var resolver = new CustomDependencyResolver(container);

            var oAuthOptions = new OAuthOptions();
            var provider = new ApplicationOAuthProvider(oAuthOptions.PublicClientId, () => resolver.Resolve<IUserService>());

            container.RegisterInstance(typeof(IOAuthAuthorizationServerProvider), provider);

            oAuthOptions.Provider = resolver.Resolve<IOAuthAuthorizationServerProvider>();

            container.RegisterInstance(typeof(OAuthOptions), oAuthOptions);
            
            return resolver;
        }
    }
}