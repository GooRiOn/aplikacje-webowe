﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using OnlineLibrary.Providers;

[assembly: OwinStartup(typeof(OnlineLibrary.Startup))]

namespace OnlineLibrary
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            
            var resolver = RegisterComponents();
            var init = resolver.Resolve<Init>();
            init.InitDb();
            ConfigureAuth(app, resolver.Resolve<OAuthOptions>());
            app.MapSignalR(new HubConfiguration());
        }
    }
}
