#  Awesome online library #

* hours needed to create that project (approximately) - 60h

### TECHNOLOGIES USED:  ###

* ASP.NET (server-side)
* AngularJs (client-side)
* TypeScript (for AngularJs)
* Entity Framework (ORM)
* SignalR ("real time" functionlity, server-side as Hub, client-side as HubProxy)
* Unity Container (IoC/ DI)


/* Created by Dariusz Pawlukiewicz */